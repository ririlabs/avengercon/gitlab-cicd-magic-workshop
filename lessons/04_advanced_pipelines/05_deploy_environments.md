# GitLab Deploy Environments and Resource Groups Tutorial

This README tutorial will guide you through setting up and utilizing Deploy Environments and Resource Groups in GitLab CI/CD. By the end of this tutorial, you will know how to manage deployment processes efficiently, ensuring that your deployments are organized and that resources are optimally used.

[TOC]

## Introduction

Deploy Environments in GitLab are used to manage and track deployments across different environments such as staging, testing, and production. Resource Groups, on the other hand, are a feature that limits the number of jobs that can run concurrently in the same environment, preventing over-deployment and ensuring that resources are not overused.

## Setting Up Deploy Environments

To set up deploy environments in GitLab, follow these steps:

1. **Define Environments in `.gitlab-ci.yml`**: Specify your environments using the `environment` keyword in your CI/CD pipeline configuration.

```yaml
deploy_to_staging:
    stage: deploy
        script:
        - echo "Deploying to staging server"
    environment:
        name: staging
        url: https://staging.example.com
```

2. **View Environments in GitLab UI**: Once the pipeline runs, you can view the environments by navigating to **CI/CD > Environments** in your project's sidebar.

## Configuring Resource Groups

Resource Groups help manage concurrent deployments. Here's how to configure them:

1. **Define Resource Groups in `.gitlab-ci.yml`**: Use the `resource_group` keyword to specify a group. Jobs in the same group will run sequentially.

```yaml
deploy_to_production:
  stage: deploy
  script:
    - echo "Deploying to production server"
  environment:
    name: production
  resource_group: production_deploy
```

2. **Ensure Sequential Deployment**: Jobs with the same `resource_group` will queue and run one after the other, preventing simultaneous deployment to the same environment.

### Example CI/CD Configuration

Here's a complete example:

```yaml
stages:
  - build
  - test
  - deploy
  - cleanup

build_job:
  stage: build
  script:
    - echo "Building application"

test_job:
  stage: test
  script:
    - echo "Running tests"

deploy_to_staging:
  stage: deploy
  script:
    - echo "Deploying to staging server"
  environment:
    name: staging
    url: https://staging.example.com

deploy_to_production:
  stage: deploy
  script:
    - echo "Deploying to production server"
  environment:
    name: production
    url: https://www.example.com
  resource_group: production_deploy
  when: manual

stop_staging:
  stage: cleanup
  script:
    - echo "Stopping the staging environment"
  environment:
    name: staging
    action: stop
  when: manual
```

## Stopping Environments

Stopping an environment in GitLab CI/CD is an important step for cleaning up resources once they are no longer needed or when a project phase comes to an end. This process helps in preventing unnecessary resource consumption and costs. Here's how to implement environment stopping in your `.gitlab-ci.yml`:

### Defining a Stop Job

To stop an environment, define a job in your CI/CD pipeline configuration that specifies the environment you wish to stop and use the `action: stop` setting under the `environment` keyword.

```yaml
stop_staging:
  stage: cleanup
  script:
    - echo "Stopping the staging environment"
  environment:
    name: staging
    action: stop
  when: manual
  only:
    - master
```

### Best Practices for Stopping Environments:

- **Timely Cleanup**: Regularly stop environments that are no longer in use to free up resources.
- **Review Before Stopping**: Ensure that all necessary data or logs are backed up or reviewed before stopping an environment.
- **Automate Where Possible**: Consider automating the stop process for temporary environments or for environments that follow a predictable lifecycle.

## Review Apps

Review Apps are a part of GitLab's CI/CD offering that allow you to spin up dynamic environments for your branches, enabling you to see your changes live without having to merge them into your main branch first. This is particularly useful for reviewing pull requests and merge requests, as it allows everyone involved to see the actual effect of changes in a live environment.

### Define the Review App in `.gitlab-ci.yml`

To set up Review Apps, you need to define a job in your `.gitlab-ci.yml` file that specifies how the app should be deployed. This includes specifying a dynamic environment for each branch.

```yaml
review:
  stage: deploy
  script: echo "Deploying a review app"
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: http://$CI_ENVIRONMENT_SLUG.example.com
```

### Configuring Environments

In the GitLab UI, you can configure environments by navigating to your project's **Settings > CI/CD** and then to the **Environments** section. Here, you can see all active environments and manage them.

### Using Review Apps

Once Review Apps are configured, they are automatically deployed for each commit to a non-main branch. You can view and interact with these environments through the merge requests associated with each branch, providing a direct way to review the impact of your changes in a live setting.

### Cleaning Up Review Apps

To ensure resources are not wasted, it's important to clean up Review Apps once they are no longer needed. This can be automated with a job in your `.gitlab-ci.yml`.

```yaml
stop_review:
  stage: cleanup
  script: echo "Stopping review app"
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  when: manual
```

## Best Practices

- **Use Descriptive Names**: For environments and resource groups, use names that clearly describe their purpose.
- **Limit Production Access**: Use `when: manual` for production deployments to require manual approval.
- **Monitor Environments**: Regularly check the Environments page in GitLab to monitor deployments and their status.
- **Resource Management**: Regularly review and clean up old or unused environments to conserve resources.
- **Security**: Ensure that Review Apps do not expose sensitive information or access to unauthorized users.
- **Documentation**: Clearly document how to access and use Review Apps for your team or contributors.

## Troubleshooting

- **Jobs Not Running Sequentially**: Ensure that jobs intended to run sequentially are assigned to the same resource group.
- **Environment Not Showing Up**: Verify that the `environment` keyword is correctly defined in your `.gitlab-ci.yml`.
- **Deployment Failures**: Check the CI/CD pipeline logs for errors during the deployment process.
- **Environment Variables**: Ensure that all necessary environment variables are correctly set for deployments.
- **Access Issues**: Verify network and firewall settings if Review Apps are not accessible.

## Conclusion

Deploy Environments and Resource Groups in GitLab CI/CD provide powerful tools for managing your deployment processes.

Review Apps in GitLab offer a dynamic and efficient way to review changes in a real-world context, enhancing the quality of the review process and facilitating better collaboration among team members.

[Overview](../../README.md) | [Back](./04_gitlab_cicd_components.md) | [Next](../05_applying_to_products/01_user_guide_creation.md)
