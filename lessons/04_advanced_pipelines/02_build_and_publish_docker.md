# Build and Publish Docker Images in CI/CD

This README provides an overview and guide on how to implement Docker-in-Docker (DinD) builds within GitLab CI/CD pipelines. Using DinD in GitLab CI/CD allows you to build, run, and push Docker images from within a Docker container. This approach is particularly useful for creating isolated build environments.

[TOC]

## Understanding Docker-in-Docker (DinD)

Docker-in-Docker (DinD) refers to the technique of running a Docker daemon inside a Docker container. This method is useful for CI/CD pipelines that require Docker commands, such as building and pushing Docker images, without installing Docker on the runner host directly.

## Security Considerations

Running DinD can introduce security concerns, as it typically requires privileged mode for the container. This can expose your host to security risks if not managed carefully. Always follow best practices for security and consider alternatives, such as Docker socket binding, if suitable for your use case.

## Configuring GitLab CI/CD for DinD

### Step 1: Update `.gitlab-ci.yml` for DinD Service

To enable DinD in your GitLab CI/CD pipelines, you must configure your `.gitlab-ci.yml` file to include the Docker-in-Docker service. Here is an example configuration:

```yaml
stages:
  - build

build_image:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t my-image:$CI_COMMIT_REF_SLUG .
    - docker push my-image:$CI_COMMIT_REF_SLUG
```

### Step 2: Use Docker Commands in Your Pipeline
With the DinD service running, you can now use Docker commands within your CI/CD script sections. The example above demonstrates logging into the Docker registry, building an image tagged with the commit reference, and pushing it to the registry.

### Step 3: Security and Permissions
Ensure your GitLab runner is configured to handle privileged containers if you're using self-hosted runners. This is necessary for the DinD service to function properly.

## Troubleshooting
- **Failed to connect to Docker daemon:** Ensure the `DOCKER_HOST` variable is correctly set to `tcp://docker:2375/`.
- **Permission Denied:** Check that your runner has the appropriate permissions to run containers in privileged mode.

## Best Practices
- **Security:** Limit the use of privileged mode and follow Docker and GitLab's security best practices.
- **Docker Caching:** To improve build times, consider strategies for Docker layer caching.
- **Clean Up:** Ensure your CI/CD jobs clean up Docker resources to prevent resource exhaustion on your runners.

## Modifying Your Pipeline to Include Image Builds
You will now implement docker builds into your pipeline. The idea here is to build images before anything else in your pipeline occurs, so that those images can be used in your pipeline. This is extremely useful when modifying build or test images that are used in your pipeline during CI/CD stages.

### Add a Docker Dockerfile
Create a new `docker` folder and add a simple `test-image.Docker` file to the folder.

```bash
mkdir docker
echo FROM ubuntu:22.04 >> docker/test-image.Docker
```

### Modify the gitlab-ci.yml
Add another downstream pipeline to the `gitlab-ci.yml` and modify it to use stages to control the order of the jobs and pipelines.

```yaml
---
variables:
  PROJECT_IMAGE: ${CI_REGISTRY}/${CI_PROJECT_PATH}/project-image:latest

jsonnet:generate:
  stage: generation
  image: ${PROJECT_IMAGE}
  script:
    - jsonnet '.gitlab/ci/gitlab-ci.jsonnet' > generated-config.yml
    - cat generated-config.yml
  artifacts:
    paths:
      - generated-config.yml

stages:
  - generation
  - pipelines

dev:pipeline:
  stage: pipelines
  needs:
    - jsonnet:generate
  trigger:
    include:
      - local: '.gitlab/ci/main.yml'      
      - artifact: generated-config.yml
        job: jsonnet:generate
    strategy: depend

docker:build:
  stage: .pre
  rules:
    - changes: 
      - docker/**
  trigger:
    include:
      - local: '.gitlab/ci/docker.yml'
    strategy: depend
```

Note that `.pre` stage occurs before any other stage in the pipeline. The `.post` stage occurs after every other stage in the pipeline. These two stages are useful for setting up and tearing down environments and requirements. We also introduce the use of the `changes` keyword, which will only trigger the pipeline on changes to the docker folder. You can further refine and restrict when the pipelines get triggered if you so wish.

### Add the docker.yml File
Create the `docker.yml` file with the following contents:

```yaml
---
stages:
  - build

workflow:
  rules:
    - if: $CI_COMMIT_TAG
      variables:
        IMAGE_VERSION: "${CI_COMMIT_TAG}"
    - if: ($CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH)
      variables:
        IMAGE_VERSION: "${CI_COMMIT_BRANCH}_${CI_COMMIT_SHORT_SHA}"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        IMAGE_VERSION: "latest"

variables:
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: "/certs"
  IMAGE_NAME: test-image
  IMAGE_PATH: ${CI_REGISTRY}/${CI_PROJECT_PATH}/${IMAGE_NAME}
  IMAGE_TAG: ${IMAGE_PATH}:${IMAGE_VERSION}

default:
  image: ${CI_REGISTRY}/${CI_PROJECT_PATH}/docker:latest
  tags:
    - docker
  services:
    - name: ${CI_REGISTRY}/${CI_PROJECT_PATH}/docker-dind:latest
      alias: docker
  before_script:
    - docker info
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

build:test-image:
  stage: build
  rules:
    - changes: 
      - docker/${IMAGE_NAME}.Docker
  script:
    - docker build -t ${IMAGE_TAG} -f docker/${IMAGE_NAME}.Docker docker
    - docker run --rm ${IMAGE_TAG} /bin/bash -c "echo 'Hello World!'"
    - docker push ${IMAGE_TAG}
```

Note that we are using a lot more pre-defined variables in here. This simplifies re-using the pipeline in multiple projects if required. We also introduce the `services` keyword, which spins up a Docker service for us to interact with and build images. Note that we have to specify the `docker` tag so that only our priviledge runner picks up these jobs. If you try to run these from an unpriviledged context, the build will fail.

## Conclusion
Docker-in-Docker in GitLab CI/CD enables powerful, containerized build environments but requires careful configuration and attention to security. This guide outlines the basics to get you started. For more detailed information, refer to the official GitLab and Docker documentation.

[Overview](../../README.md) | [Back](./01_scheduled_pipelines.md) | [Next](./03_build_and_publish_python.md)
