local build(arch) =
{
  extends: '.build',
  variables: { PACKAGE_ARCH: arch }
};

local analysis(arch) =
{
  extends: '.analysis',
  needs: [['build:' + arch]],
  variables: { PACKAGE_ARCH: arch} 
};

local test(arch) =
{
  extends: '.test',
  needs: [['build:' + arch]],
  variables: { PACKAGE_ARCH: arch }
};

local package(arch) =
{
  extends: '.package',
  needs: [['build:' + arch], ['analysis:' + arch], ['test:' + arch]],
  variables: { PACKAGE_ARCH: arch }
};

local archs = ['x86_64', 'aarch64', 'mips', 'mipsel'];

{
  ['build:' + x]: build(x) for x in archs
}

{
  ['analysis:' + x]: analysis(x) for x in archs
}

{
  ['test:' + x]: test(x) for x in archs
}

{
  ['package:' + x]: package(x) for x in archs
}
