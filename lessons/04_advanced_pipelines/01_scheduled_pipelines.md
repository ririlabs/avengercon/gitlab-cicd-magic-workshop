# Using Scheduled Pipelines

This tutorial will guide you through the process of setting up scheduled pipelines in GitLab CI/CD, allowing you to automate your testing, building, and deployment processes based on a schedule you define.

[TOC]

## Scheduling a Pipeline

1. **Navigate to CI/CD Schedules**: Go to your project on GitLab. Under the **Build** menu, select **Pipeline Schedules**.

2. **Add a New Schedule**: Click the **New schedule** button. You will be prompted to define the schedule details.

3. **Fill Schedule Details**:
    - **Description**: Give your schedule a meaningful description.
    - **Interval Pattern**: Choose how often your pipeline will run. You can select predefined intervals (like nightly or monthly) or use cron syntax for more control.
    - **Target Branch**: Select the branch you want the pipeline to run against.
    - **Variables** (Optional): Define any environment variables needed specifically for this schedule.

4. **Save Schedule**: After setting up your schedule details, click **Save pipeline schedule**.

## Configuring `.gitlab-ci.yml` for Scheduled Pipelines

You might want certain jobs to run only during scheduled pipelines and not on every push. Use the `only` or `except` keywords with `schedules` to control this:

```yaml
scheduled_job:
  script:
    - echo "This job runs only on a schedule."
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"


regular_job:
  script:
    - echo "This job runs on every push but not on schedules."
  rules:
    - if: $CI_PIPELINE_SOURCE != "schedule"
```

## Monitoring Scheduled Pipelines
Once your pipeline is scheduled, it will run automatically at the specified intervals. To monitor these, go back to the Schedules page under the CI/CD menu. You'll see the next run time for each schedule and can view past runs by clicking on them.

## Conclusion
Scheduled pipelines are a powerful feature in GitLab CI/CD that allows you to automate your workflows based on time-based triggers. By following the steps outlined in this tutorial, you can easily set up and manage scheduled pipelines for your projects.

[Overview](../../README.md) | [Back](../03_optimizing_and_maintanability/06_dynamic_pipelines.md) | [Next](./02_build_and_publish_docker.md)
