# GitLab CICD Components

A CI/CD component is a reusable single pipeline configuration unit. Use components to create a small part of a larger pipeline, or even to compose a complete pipeline configuration. A component can be configured with input parameters for more dynamic behavior.

[TOC]

## Introduction
CI/CD components are similar to the other kinds of configuration added with the `include` keyword, but have several advantages:

- Components can be listed in the CI/CD Catalog.
- Components can be released and used with a specific version.
- Multiple components can be defined in the same project and versioned together.

## Spec Inputs
Use `spec:inputs` to define input parameters for CI/CD configuration intended to be added to a pipeline with `include`. Use `include:inputs` to pass input values when building the configuration for a pipeline.

The specs must be declared at the top of the configuration file, in a header section. Separate the header from the rest of the configuration with ---.

Use the interpolation format `$[[ inputs.input-id ]]` outside the header section to replace the values. The inputs are evaluated and interpolated when the configuration is fetched during pipeline creation, but before the configuration is merged with the contents of the `.gitlab-ci.yml` file.

For example, in a file named `custom_website_scan.yml`:

```yaml
spec:
  inputs:
    job-stage:
    environment:
---

scan-website:
  stage: $[[ inputs.job-stage ]]
  script: ./scan-website $[[ inputs.environment ]]
```

When using spec:inputs:

- Inputs are mandatory by default.
- Validation errors are returned if:
  - A string containing an interpolation block exceeds 1 MB.
  - The string inside an interpolation block exceeds 1 KB.

Additionally, use:

- `spec:inputs:default` to define default values for inputs when not specified. When you specify a default, the inputs are no longer mandatory.
- `spec:inputs:description` to give a description to a specific input. The description does not affect the input, but can help people understand the input details or expected values.
- `spec:inputs:options` to specify a list of allowed values for an input.
- `spec:inputs:regex` to specify a regular expression that the input must match.
- `spec:inputs:type` to force a specific input type, which can be string (default when not specified), number, or boolean.

## Create a Component Project
To create and use a component project, you must:

- Create a new project with a README.md file:
  - Ensure the description gives a clear introduction to the component.
  - Components published to the CI/CD catalog use both the description and avatar when displaying the component project’s summary.
- Add a YAML configuration file for each component, following the required directory structure. For example:

```yaml
spec:
  inputs:
    stage:
      default: test
---
component-job:
  script: echo job 1
  stage: $[[ inputs.stage ]]
```
- Release the component project. You should use a release stage to automatically release on tags:

```yaml
create-release:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script: echo "Creating release $CI_COMMIT_TAG"
  rules:
    - if: $CI_COMMIT_TAG
  release:
    tag_name: $CI_COMMIT_TAG
    description: "Release $CI_COMMIT_TAG of components in $CI_PROJECT_PATH"
```

## Directory structure
The repository must contain:

- A README.md Markdown file documenting the details of all the components in the repository.
- A top level templates/ directory that contains all the component configurations. You can define components in this directory:
- In single files ending in .yml for each component, like templates/secret-detection.yml.
- In sub-directories containing template.yml files as entry points, for components that bundle together multiple related files. For example, templates/secret-detection/template.yml.

## Using Components in Your Pipeline
You will now modify your pipeline to use components. 

### Components Project
First create a new `components` project under `developer`. Then create the following folder structure and files:

```shell
.
├── README.md
└── templates
    ├── clang-format-linting
    │   └── template.yml
    └── clang-tidy-analysis
        └── template.yml
└── .gitlab-cy.yml
```

You will need to create and publish several Docker images for this project:

```bash
sudo docker build -t gitlab.home.arpa:5050/developer/components/project-image:latest -f ./docker/project-image.Dockerfile docker
sudo docker build -t gitlab.home.arpa:5050/developer/components/gitlab-release-cli:latest -f ./docker/gitlab-release-cli.Dockerfile docker

sudo docker push gitlab.home.arpa:5050/developer/components/project-image:latest
sudo docker push gitlab.home.arpa:5050/developer/components/gitlab-release-cli:latest
```

#### clang-format-linting: template.yml

```yaml
spec:
  inputs:
    image:
      default: ${CI_REGISTRY}/${CI_PROJECT_PATH}/project-image:latest
    stage:
      default: lint
    find_directory:
      default: "."
    find_options:
      default: "-iname '*.c' -o -iname '*.h'"
    format_style:
      default: microsoft
---

lint:clang-format:
  image: $[[ inputs.image ]]
  stage: $[[ inputs.stage ]]
  script:
    - find $[[ inputs.find_directory ]] $[[ inputs.find_options ]] | xargs clang-format -i --style=$[[ inputs.format_style ]]
    - git diff --exit-code || (echo "Code style issues found. Please run 'clang-format' locally." && exit 1)
```

#### clang-tidy-analysis: template.yml

```yaml
spec:
  inputs:
    image:
      default: ${CI_REGISTRY}/${CI_PROJECT_PATH}/project-image:latest
    stage:
      default: analysis
    find_directory:
      default: "."
    find_options:
      default: "-iname '*.c' -o -iname '*.h'"
    ctidy_checks: 
      default: "*,-clang-analyzer-alpha.*,-llvmlibc-restrict-system-libc-headers"
    ctidy_database:
      default: "."
---

.analysis:clang-tidy:
  image: $[[ inputs.image ]]
  stage: $[[ inputs.stage ]]
  script:
    - find $[[ inputs.find_directory ]] $[[ inputs.find_options ]] | xargs clang-tidy -p $[[ inputs.ctidy_database ]] --checks="$[[ inputs.ctidy_checks ]]"
```

#### gitlab-ci.yml

```yaml
---
create-release:
  stage: deploy
  image: ${CI_REGISTRY}/${CI_PROJECT_PATH}/gitlab-release-cli:latest
  script: echo "Creating release $CI_COMMIT_TAG"
  rules:
    - if: $CI_COMMIT_TAG
  release:
    tag_name: $CI_COMMIT_TAG
    description: "Release $CI_COMMIT_TAG of components in $CI_PROJECT_PATH"
```

#### Tag to create a Release
After you are done setting up the project, simply add a tag to release the components so they can be used.

### Update the Workshop Project
Now that you have components ready to use, let's update the pipeline to use these. You can safely delete the `lint.yml` and `analysis.yml` files from your workshop repository. Then modify the `gitlab-ci.jsonnet` and `main.yml` files.

#### gitlab-ci.jsonnet

```json
local build(arch) =
{
  extends: '.build',
  variables: { PACKAGE_ARCH: arch }
};

local analysis(arch) =
{
  extends: '.analysis:clang-tidy',
  needs: [['build:' + arch]],
  variables: { PACKAGE_ARCH: arch} 
};

local test(arch) =
{
  extends: '.test',
  needs: [['build:' + arch]],
  variables: { PACKAGE_ARCH: arch }
};

local package(arch) =
{
  extends: '.package',
  needs: [['build:' + arch], ['analysis:' + arch], ['test:' + arch]],
  variables: { PACKAGE_ARCH: arch }
};

local archs = ['x86_64', 'aarch64', 'mips', 'mipsel'];

{
  ['build:' + x]: build(x) for x in archs
}

{
  ['analysis:' + x]: analysis(x) for x in archs
}

{
  ['test:' + x]: test(x) for x in archs
}

{
  ['package:' + x]: package(x) for x in archs
}
```

#### main.yml

```yaml
---
variables:
  PROJECT_IMAGE: ${CI_REGISTRY}/${CI_PROJECT_PATH}/project-image:latest
  GITLAB_CLI_IMAGE: ${CI_REGISTRY}/${CI_PROJECT_PATH}/gitlab-release-cli:latest
  PYTHON_PACKAGE_NAME: 'python-package'
  PYTHON_PACKAGE_VERSION: '0.0.0'

stages:
  - lint
  - build
  - analysis
  - test
  - package
  - release

workflow:
  rules:
    - if: $CI_COMMIT_TAG
      variables:
        PACKAGE_VERSION: '${CI_COMMIT_TAG}'
    - if: ($CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH)
      variables:
        PACKAGE_VERSION: "${CI_COMMIT_BRANCH}_${CI_COMMIT_SHORT_SHA}"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        PACKAGE_VERSION: "latest"
    - if: $CI_COMMIT_TAG =~ /^(\d+\.)?(\d+\.)?(\*|\d+)$/
      variables:
        PYTHON_PACKAGE_VERSION: '${CI_COMMIT_TAG}'

.base:
  image: $PROJECT_IMAGE

include:
  - component: gitlab.home.arpa/developer/components/clang-format-linting@1.0.0
    inputs:
      stage: lint
      find_directory: ./src
  - component: gitlab.home.arpa/developer/components/clang-tidy-analysis@1.0.0
    inputs:
      stage: analysis
      find_directory: ./src
      ctidy_database: build-${PACKAGE_ARCH}
  - local: .gitlab/ci/build.yml
  - local: .gitlab/ci/test.yml
  - local: .gitlab/ci/package.yml
  - local: .gitlab/ci/release.yml
```

#### Push Your Changes
You should now see the pipeline running using GitLab CI/CD Components.

[Overview](../../README.md) | [Back](./03_build_and_publish_python.md) | [Next](./05_deploy_environments.md)
