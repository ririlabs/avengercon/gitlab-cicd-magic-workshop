# Build and Publish Python Packages in CI/CD

This tutorial guides you through the process of automating the building and publishing of Python wheel packages to the GitLab Python package registry within a CI/CD pipeline.

[TOC]

## Prerequisites

Before you begin, make sure you have:

- A GitLab account and a project repository for your Python package.
- GitLab CI/CD enabled for your project.
- A `pyproject.toml` or `setup.py` file in your project root that defines your package metadata and dependencies.

## Prepare Your Python Project

Your project should have a proper structure and include a `pyproject.toml` or `setup.py` file. Here's an example project layout with `pyproject.toml`:

```shell
my_package/
├── my_package/
│ ├── init.py
│ └── module.py
├── tests/
│ └── test_module.py
└── pyproject.toml
```

Example `pyproject.toml`:

```toml
[build-system]
requires = ["setuptools>=42", "wheel"]
build-backend = "setuptools.build_meta"

[project]
name = "my_package"
version = "0.1.0"
description = "A sample Python project"
authors = [{name = "Your Name", email = "you@example.com"}]
```

## Configure GitLab CI/CD Pipeline
Create a `.gitlab-ci.yml` file at the root of your project to define your CI/CD pipeline:

```yaml
Copy code
stages:
  - build
  - publish

build_wheel:
  stage: build
  image: python:3.9
  script:
    - pip install build
    - python -m build
  artifacts:
    paths:
      - dist/*.whl
    expire_in: 1 hour

publish_to_gitlab:
  stage: publish
  image: python:3.9
  script:
    - pip install twine
    - echo "__token__:${CI_JOB_TOKEN}" > .pypirc
    - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*.whl
```

This pipeline builds your wheel package and publishes it to the GitLab Python package registry.

## Commit and Push Changes
Commit and push your `pyproject.toml` and `.gitlab-ci.yml` files to trigger the CI/CD pipeline.

## Installing Your Package
Install your package directly from your GitLab registry using pip:

```bash
pip install --extra-index-url https://<gitlab-host>/api/v4/projects/<project-id>/packages/pypi/ my_package
```

Replace `<gitlab-host>` and `<project-id>s` with your GitLab instance's host and your project's ID, respectively.

## Adding Python Package to Your Project
Let's expand our pipeline now to incorporate pyhthon builds and releases.

### Add a Python Project
Re-arrange your `src` directory and add following python project structure under `src`

```shell
cprog/
├── cprog
| └── main.c 
python-package/
├── my_package/
│ ├── init.py
│ └── module.py
├── tests/
│ └── test_module.py
└── pyproject.toml
```

### Modify Your `main.yml`
Add Python package information variables to the `main.yml` to be used in your pipeline.

```yaml
---
variables:
  PROJECT_IMAGE: ${CI_REGISTRY}/${CI_PROJECT_PATH}/project-image:latest
  GITLAB_CLI_IMAGE: ${CI_REGISTRY}/${CI_PROJECT_PATH}/gitlab-release-cli:latest
  PYTHON_PACKAGE_NAME: 'python-package'
  PYTHON_PACKAGE_VERSION: '0.0.0'

stages:
  - lint
  - build
  - analysis
  - test
  - package
  - release

workflow:
  rules:
    - if: $CI_COMMIT_TAG
      variables:
        PACKAGE_VERSION: '${CI_COMMIT_TAG}'
    - if: ($CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH)
      variables:
        PACKAGE_VERSION: "${CI_COMMIT_BRANCH}_${CI_COMMIT_SHORT_SHA}"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        PACKAGE_VERSION: "latest"
    - if: $CI_COMMIT_TAG =~ /^(\d+\.)?(\d+\.)?(\*|\d+)$/
      variables:
        PYTHON_PACKAGE_VERSION: '${CI_COMMIT_TAG}'

.base:
  image: $PROJECT_IMAGE

include:
  - local: .gitlab/ci/lint.yml
  - local: .gitlab/ci/build.yml
  - local: .gitlab/ci/analysis.yml
  - local: .gitlab/ci/test.yml
  - local: .gitlab/ci/package.yml
  - local: .gitlab/ci/release.yml
```

### Add a Python Build Job
Modify the `build.yml` to include a build job for the python package. Note we are packaging / building with `wheel` and `pyinstaller` respectively.

```yaml
build:python:
  extends: .base
  stage: build
  needs: []
  script:
    - mkdir -p out/dist/bin
    - sed -i '/version = "0.0.0"/c\version = "'$(eval echo ${PYTHON_PACKAGE_VERSION})'"' src/${PYTHON_PACKAGE_NAME}/pyproject.toml
    - cat src/${PYTHON_PACKAGE_NAME}/pyproject.toml
    - python3 -m build src/${PYTHON_PACKAGE_NAME}
    - pyinstaller --clean -F --distpath out/dist/bin src/${PYTHON_PACKAGE_NAME}/${PYTHON_PACKAGE_NAME}/module.py --name ${PYTHON_PACKAGE_NAME}
    - cp src/${PYTHON_PACKAGE_NAME}/dist/*.whl out/dist
  artifacts:
    paths:
      - out
    expire_in: 1 week
```

### Modify you `release.yml` to Accomodate Python Packages
We will need a `package:all` job to ensure all of our artifacts are placed under one .tar.gz for release. We will also need to take care of existing Python packages in the PyPi registry with the same version (e.g. 0.0.0) before uploading new ones. Python packages must follow semantic versioning, so the `latest` tag is defined as version `0.0.0`.

```yaml
---
.release:base:
  stage: release
  image: $GITLAB_CLI_IMAGE
  variables:
    TAR_NAME: ${CI_PROJECT_NAME}-${PACKAGE_VERSION}.tar.gz

package:all:
  extends: .release:base
  script:
    - tar -cvf ${TAR_NAME} -C out/ .
  artifacts:
    paths:
      - ${TAR_NAME}
      - out/dist
    expire_in: 1 week

release:
  extends: .release:base
  rules:
    - if: $CI_COMMIT_TAG =~ /^(\d+\.)?(\d+\.)?(\*|\d+)$/
      variables:
        CHANGELOG_GET_COMMAND: 'curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X GET "${PROJECT_API}/repository/changelog?version=${CI_COMMIT_TAG}" | jq -r .notes > release_notes.md'
        CHANGELOG_POST_COMMAND: 'curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X POST "${PROJECT_API}/repository/changelog?version=${CI_COMMIT_TAG}"'
        REMOVE_TAG_COMMAND: 'true'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        CHANGELOG_GET_COMMAND: 'echo "This is the latest build for the project repository. It contains no changelog." > release_notes.md'
        CHANGELOG_POST_COMMAND: 'echo "No changelog update needed, this is the latest build..."'
        REMOVE_TAG_COMMAND: 'echo "Removing previous matching tag if found..." && curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X DELETE "${PROJECT_API}/repository/tags/${PACKAGE_VERSION}"'
  needs: [package:all]
  variables:
    PROJECT_API: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}
    PACKAGE_REGISTRY_GENERIC_URL: ${PROJECT_API}/packages/generic
  script:
    - eval "${REMOVE_TAG_COMMAND}"
    - |
      echo "Creating changelog..."
      eval "${CHANGELOG_GET_COMMAND}" && eval "${CHANGELOG_POST_COMMAND}"
    - | 
      echo "Uploading generic packages..."
      curl -H "JOB-TOKEN:${CI_JOB_TOKEN}" --upload-file "${TAR_NAME}" "${PACKAGE_REGISTRY_GENERIC_URL}/${CI_PROJECT_NAME}/${PACKAGE_VERSION}/${TAR_NAME}"
    - |
      echo "Deleting old Python packages..."
      curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X GET "${PROJECT_API}/packages" -o packages.json
      eval "jq '.[] | select(.name==\"${PYTHON_PACKAGE_NAME}\" and .version==\"${PYTHON_PACKAGE_VERSION}\") | .id' packages.json | tee pkgid"
      export PACKAGE_ID=$(cat pkgid)
      curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X DELETE "${PROJECT_API}/packages/$PACKAGE_ID"
    - |
      echo "Publishing Python packages..."
      TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token twine upload --repository-url ${PROJECT_API}/packages/pypi out/dist/*.whl
    - echo "Creating release..."
  release:
    name: '$CI_PROJECT_NAME:$PACKAGE_VERSION'
    description: release_notes.md
    tag_name: '$PACKAGE_VERSION'
    ref: '$CI_COMMIT_SHA'
    assets:
      links:
        - name: '${TAR_NAME}'
          url: "${PACKAGE_REGISTRY_GENERIC_URL}/${CI_PROJECT_NAME}/${PACKAGE_VERSION}/${TAR_NAME}"
```

## Conclusion
You've automated the building and publishing of Python wheel packages to GitLab's Python package registry using CI/CD. This ensures consistent builds and distribution, making it easier for users to install your package.

[Overview](../../README.md) | [Back](./02_build_and_publish_docker.md) | [Next](./04_gitlab_cicd_components.md)
