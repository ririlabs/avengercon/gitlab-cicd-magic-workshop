# Environment Setup
This section will cover initial setup for the workshop.

[TOC]

## System Requirements
- You will require an internet connection.
- You will need an Ubuntu 22.04 LTS Virtual Machine for this workshop.

> **_WARNING_**: This workshop including its setup scripts have only been tested on Ubuntu 22.04 LTS on x86_64 on VirtualBox. Your milage may vary on other systems and architectures!

> **_NOTE_**: It is highly recommended to give this Virtual Machine ample resources, as it will be hosting a GitLab instance, GitLab runners, and your development environment.

## Setting up the Environment

Checkout or download the contents of this repository to a directory you have read/write permissions to in your VM, then follow the directions below:

1. Go into the `scripts` folder.
   - `cd scripts`

1. Run the `setup-environment.sh` script and wait for it to finish.
   - `./setup-environment.sh`

>**_NOTE_**: If you are running an `aarch64` (ARM) Ubuntu 22.04 VM (e.g. on MacOS), you should modify the image used in the docker-compose file to use `zengxs/gitlab:latest`, which is a community built image for GitLab on arm64. For your convenience, an `aarch64` docker compose file is provided. You'll need to modify the setup script to use it before continuing. If you are running an `amd64` arch, no changes are needed.

1. Run the `setup-gitlab.sh` script and wait for it to finish. Take note of the initial root password that is printed as you will need this.
   - `./setup-gitlab.sh`

1. Firefox does not use the system CA store, so you'll have to import the CA certificate manually via the Firefox GUI. The CA certificate should be located at `scripts/ssl/`. 
   - Go to `Settings->Privacy and Security->View Certificates->Import` and select the `scripts/ssl/gitlab.home.arpa.CA.crt` certificate.

> **_NOTE_**: GitLab can take around five minutes to fully come up.

## Setting up the GitLab Runner

Now that you have a fully running GitLab instance, the next step is to setup a GitLab Runner. GitLab Runner is an application that works with GitLab CI/CD to run jobs in a pipeline.
1. Go to `https://gitlab.home.arpa/` and login as `root` with the initial root password provided.
1. Now navigate to `https://gitlab.home.arpa/admin/runners/new` and add the following tags: `c, python, linting, analysis, release`.
1. Click on `Create` and copy the runner authentication token provided.
1. Run the `setup-runners.sh` script with the runner authentication token as an argument:
   - `./setup-runner.sh gitlab-runner-one <runner_authentication_token>`
1. Repeat the first few steps to setup a second runner token and then run the command:
   - `./setup-runner.sh gitlab-runner-two <runner_authentication_token>`

## Other Useful Steps
Congratulations! You now have a fully functional GitLab setup. You should take the following steps to make your GitLab experience a bit smoother:
- Create a user account so you don't have to login as root.
  - `https://gitlab.home.arpa/admin/users/new`
  > **_NOTE_**: Recommend creating the account `developer` for the purposes of this workshop.
- Turn off sign-ups to get rid of the warning banner.
  - `https://gitlab.home.arpa/admin/application_settings/general#js-signup-settings`
- Setup ssh and GPG keys.
- Create a `Project Access Token` with `owner` scope and `api` permissions. Add the project token value to the CI/CD variables in your project under the name `PROJECT_API_TOKEN`.
- Create a project under your workspace.
  - `https://gitlab.home.arpa/projects/new#blank_project`
  > **_NOTE_**: Recommend using the project name `workshop` under the `developer` namespace for the purposes of this workshop.

- This workshop repository contains Dockerfiles that you will need to create the images used in pipelines. From the root directory of this repository, use the following commands to build and upload your images:

> **_NOTE_**: Replace <username> and <project-name> with your GitLab username and project name, respectively. It is highly recommended to use `developer` and `workshop` as the user name and project names for the purposes of this workshop. Enter your GitLab password when prompted.
```bash
sudo docker login gitlab.home.arpa:5050 -u <username>

sudo docker build -t gitlab.home.arpa:5050/<username>/<project-name>/project-image:latest -f ./docker/project-image.Dockerfile docker
sudo docker build -t gitlab.home.arpa:5050/<username>/<project-name>/gitlab-release-cli:latest -f ./docker/gitlab-release-cli.Dockerfile docker
sudo docker build -t gitlab.home.arpa:5050/<username>/<project-name>/docker:latest -f ./docker/docker.Dockerfile docker
sudo docker build -t gitlab.home.arpa:5050/<username>/<project-name>/docker-dind:latest -f ./docker/docker-dind.Dockerfile docker
sudo docker build -t gitlab.home.arpa:5050/<username>/<project-name>/pandoc:latest -f docker/pandoc.Dockerfile docker

sudo docker push gitlab.home.arpa:5050/<username>/<project-name>/project-image:latest
sudo docker push gitlab.home.arpa:5050/<username>/<project-name>/gitlab-release-cli:latest
sudo docker push gitlab.home.arpa:5050/<username>/<project-name>/docker:latest
sudo docker push gitlab.home.arpa:5050/<username>/<project-name>/docker-dind:latest
sudo docker push gitlab.home.arpa:5050/<username>/<project-name>/pandoc:latest
```

> **_NOTE_**: Using recommended username and project name:
```bash
sudo docker login gitlab.home.arpa:5050 -u developer

sudo docker build -t gitlab.home.arpa:5050/developer/workshop/project-image:latest -f ./docker/project-image.Dockerfile docker
sudo docker build -t gitlab.home.arpa:5050/developer/workshop/gitlab-release-cli:latest -f ./docker/gitlab-release-cli.Dockerfile docker
sudo docker build -t gitlab.home.arpa:5050/developer/workshop/docker:latest -f ./docker/docker.Dockerfile docker
sudo docker build -t gitlab.home.arpa:5050/developer/workshop/docker-dind:latest -f ./docker/docker-dind.Dockerfile docker
sudo docker build -t gitlab.home.arpa:5050/developer/workshop/pandoc:latest -f docker/pandoc.Dockerfile docker

sudo docker push gitlab.home.arpa:5050/developer/workshop/project-image:latest
sudo docker push gitlab.home.arpa:5050/developer/workshop/gitlab-release-cli:latest
sudo docker push gitlab.home.arpa:5050/developer/workshop/docker:latest
sudo docker push gitlab.home.arpa:5050/developer/workshop/docker-dind:latest
sudo docker push gitlab.home.arpa:5050/developer/workshop/pandoc:latest
```

After completeing these steps, you will be ready to start the workshop itself.

[Overview](../../README.md) | [Next](./02_development_philosophy.md)
