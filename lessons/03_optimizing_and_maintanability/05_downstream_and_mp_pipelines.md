# Comprehensive Guide to GitLab CI/CD Downstream Pipelines

This guide focuses on setting up downstream pipelines within GitLab CI/CD, including internal downstream pipelines and multi-project pipelines.

[TOC]

## Introduction

Downstream pipelines are a powerful feature in GitLab CI/CD that allows you to trigger a pipeline from another pipeline. These can be within the same project (internal downstream) or across different projects (cross-project downstream), facilitating complex workflows and modular CI/CD processes.

The trigger keyword in GitLab CI/CD is a powerful feature used to define downstream pipeline triggers within your .gitlab-ci.yml configuration file. This functionality enables one pipeline to automatically trigger another pipeline, which can be within the same project (internal downstream) or in a different project (cross-project downstream). The trigger keyword facilitates modular, scalable, and efficient CI/CD workflows by allowing separate stages or processes to be handled by distinct pipelines.

## How the `trigger` Keyword Works
When you use the `trigger` keyword in a job within your `.gitlab-ci.yml` file, GitLab CI/CD interprets this as an instruction to trigger another pipeline as part of the job's execution. The downstream pipeline is triggered after the job's script section completes successfully.

### Basic Usage
Here's a basic example of how to use the `trigger` keyword to trigger a downstream pipeline within the same project:

```yaml
deploy:
  stage: deploy
  trigger:
    include: .gitlab-deploy.yml
```

In this example, the `deploy` job triggers a downstream pipeline defined in the `.gitlab-deploy.yml` file, which is included in the same project.

### Cross-Project Triggers
To trigger a pipeline in a different project, you specify the project's path and, optionally, the branch you want to trigger:

```yaml
trigger_downstream:
  stage: deploy
  trigger:
    project: my-group/my-other-project
    branch: main
```

This example triggers a pipeline in the `main` branch of the my-group/my-other-project repository at the deploy stage.

### Passing Variables
You can also pass variables to the downstream pipeline using the trigger keyword:

```yaml
trigger_downstream:
  stage: deploy
  trigger:
    project: my-group/my-other-project
    branch: master
    strategy: depend
  variables:
    DEPLOY_ENVIRONMENT: production
```

In this configuration, the `DEPLOY_ENVIRONMENT` variable is passed to the downstream pipeline, allowing you to dynamically alter the behavior of the downstream pipeline based on the upstream pipeline's context.

### Strategy Option
The `strategy` option under the `trigger` keyword defines how the downstream pipeline's status affects the upstream pipeline. Currently, GitLab supports the `depend` strategy:

```yaml
trigger_downstream:
  stage: deploy
  trigger:
    include: .gitlab-deploy.yml
    strategy: depend
```

With strategy: depend, the upstream pipeline will wait for the downstream pipeline to complete. The upstream pipeline's status will depend on the completion status of the downstream pipeline—if the downstream pipeline fails, the upstream pipeline will also be marked as failed.

### Trigger Forward Options
Use `trigger:forward` to specify what to forward to the downstream pipeline. You can control what is forwarded to both parent-child pipelines and multi-project pipelines.

Possible inputs:

- **yaml_variables:** true (default), or false. When true, variables defined in the trigger job are passed to downstream pipelines.
- **pipeline_variables:** true or false (default). When true, manual pipeline variables and scheduled pipeline variables are passed to downstream pipelines.

Example:

```yaml
variables: # default variables for each job
  VAR: value

# Default behavior:
# - VAR is passed to the child
# - MYVAR is not passed to the child
child1:
  trigger:
    include: .child-pipeline.yml

# Forward pipeline variables:
# - VAR is passed to the child
# - MYVAR is passed to the child
child2:
  trigger:
    include: .child-pipeline.yml
    forward:
      pipeline_variables: true

# Do not forward YAML variables:
# - VAR is not passed to the child
# - MYVAR is not passed to the child
child3:
  trigger:
    include: .child-pipeline.yml
    forward:
      yaml_variables: false
```

### Use Cases
The `trigger` keyword enables a variety of CI/CD workflows, including but not limited to:

- **Microservices Deployment:** Trigger separate deployment pipelines for different microservices stored in the same or different projects.
- **Multi-Stage Deployment:** Trigger deployment pipelines for different environments (development, staging, production) in a controlled sequence.
- **Modular Testing:** Trigger specialized testing pipelines (integration, performance, security) after a successful build phase.

The `trigger` keyword's versatility makes it an essential tool for designing complex, multi-faceted CI/CD pipelines in GitLab, allowing teams to automate workflows efficiently and securely across projects and stages.

### Trigger Tokens
Trigger tokens in GitLab CI/CD are unique, secret tokens used to trigger pipelines for a project from external sources securely. These tokens allow you to initiate CI/CD pipelines through GitLab's API without exposing your personal authentication information or requiring direct Git repository access. This feature is particularly useful for automating workflows that integrate with external systems, such as deploying updates after a successful external build process or integrating with third-party automation tools.

#### How Trigger Tokens Work
Each project in GitLab can generate its own set of CI/CD trigger tokens. These tokens are used in conjunction with the GitLab API to trigger pipelines. When you make an API call to trigger a pipeline, you include the token in the request. GitLab authenticates the request based on this token and starts the specified pipeline.

#### Creating a Trigger Token
To create a new trigger token in GitLab, follow these steps:

- Navigate to your project's homepage on GitLab.
- Go to Settings > CI / CD and expand the Pipeline triggers section.
- Click Add trigger.
- Enter a description for the trigger (optional) and click Create trigger. This action generates a new trigger token.
- Once created, GitLab displays the trigger token. You should copy and securely store this token, as it won't be shown again.

#### Using a Trigger Token to Trigger a Pipeline
After creating a trigger token, you can use it to trigger a pipeline via a POST request to GitLab's API. Here's an example using curl:

```bash
curl -X POST \
     -F token=TOKEN \
     -F ref=REF_NAME \
     "https://gitlab.com/api/v4/projects/PROJECT_ID/trigger/pipeline"
```

Replace `TOKEN` with your trigger token, `REF_NAME` with the name of the branch or tag you want to trigger the pipeline on, and `PROJECT_ID` with your project's ID.

#### Passing Variables with Trigger Tokens
You can also pass variables to the pipeline when triggering it with a token:

```bash
curl -X POST \
     -F token=TOKEN \
     -F ref=REF_NAME \
     -F "variables[KEY]=VALUE" \
     "https://gitlab.com/api/v4/projects/PROJECT_ID/trigger/pipeline"
```

Replace `KEY` and `VALUE` with the desired variable name and value. You can include multiple variables by adding additional -F "variables[KEY]=VALUE" parameters to the command.


#### Security Considerations
- **Keep Trigger Tokens Secret:** Trigger tokens should be treated as sensitive data and stored securely. Exposure of a trigger token could allow unauthorized users to trigger pipelines in your project.
- **Use Scoped Tokens:** Use tokens that have the minimum required permissions for their intended purpose. Limit the scope of actions that can be performed with each token.
- **Rotate Tokens Regularly:** Regularly rotate your trigger tokens to reduce the risk of misuse if a token is compromised.

## Applying Downstream Pipelines to Your Pipeline

First, we will create a `main.yml` file in the `.gitlab/ci` folder and move over all of the contents from the original `.gitlab-ci.yml` file.

```yaml
variables:
  PROJECT_IMAGE: gitlab.home.arpa:5050/developer/workshop/project-image:latest
  GITLAB_CLI_IMAGE: gitlab.home.arpa:5050/developer/workshop/gitlab-release-cli:latest
  PROJECT_API: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}
  PACKAGE_REGISTRY_GENERIC_URL: ${PROJECT_API}/packages/generic
  PACKAGE_ARCH: generic
  PACKAGE_NAME: ${CI_PROJECT_NAME}-${PACKAGE_VERSION}-${PACKAGE_ARCH}

stages:
  - lint
  - build
  - analysis
  - test
  - package
  - release

workflow:
  rules:
    - if: $CI_COMMIT_TAG
      variables:
        PACKAGE_VERSION: "${CI_COMMIT_TAG}"
    - if: ($CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH)
      variables:
        PACKAGE_VERSION: "${CI_COMMIT_BRANCH}_${CI_COMMIT_SHORT_SHA}"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        PACKAGE_VERSION: "latest"

.base:
  image: $PROJECT_IMAGE

include:
  - local: .gitlab/ci/lint.yml
  - local: .gitlab/ci/build.yml
  - local: .gitlab/ci/analysis.yml
  - local: .gitlab/ci/test.yml
  - local: .gitlab/ci/package.yml
  - local: .gitlab/ci/release.yml
```

Next we modify our `.gitlab-ci.yml` file to look like the following:

```yaml
---
trigger:pipeline:
  trigger:
    include:
      - local: '.gitlab/ci/main.yml'
    strategy: depend
```

Now you will have your entire pipeline as a downstream pipeline. This will be useful later.

## Conclusion
Downstream pipelines in GitLab CI/CD offer a powerful and flexible way to structure and manage complex workflows across single or multiple projects. By enabling one pipeline to automatically trigger another, teams can create a modular CI/CD setup that separates concerns, reduces build times, and manages dependencies more effectively. This capability is crucial for implementing advanced deployment strategies, such as canary deployments, feature toggles, or multi-environment deployments, and for supporting microservices architectures where different components may be developed and deployed independently.

The use of trigger tokens with the GitLab API further enhances security and automation possibilities, allowing for pipelines to be triggered from external systems without compromising security. This facilitates a seamless integration of GitLab CI/CD pipelines into broader automation workflows, enabling teams to respond more quickly to changes, reduce manual errors, and improve the overall efficiency and reliability of the development process.

In conclusion, mastering downstream pipelines and effectively leveraging GitLab's CI/CD features can significantly improve your team's ability to deliver software rapidly, reliably, and securely. Whether you're working within a single project or orchestrating a complex workflow across multiple projects, GitLab provides the tools necessary to build a robust CI/CD pipeline that meets the needs of modern software development practices.

[Overview](../../README.md) | [Back](./04_using_parallel.md) | [Next](./06_dynamic_pipelines.md)
