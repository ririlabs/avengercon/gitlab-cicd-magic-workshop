# Using the `extends` Keyword and `!reference` Tags

This README offers a comprehensive guide on the `extends` keyword and `!reference` tag in GitLab CI/CD, including their functionalities, use cases, advantages, and disadvantages. Both features are designed to enhance the reusability and maintainability of `.gitlab-ci.yml` configurations.

[TOC]

## The `extends` Keyword

The `extends` keyword in GitLab CI/CD is used to inherit configurations from one job to another, allowing for the reuse of script definitions, variables, and other job configurations. This feature helps reduce duplication and keeps the CI/CD configuration DRY (Don't Repeat Yourself).

### How to Use `extends`

To use `extends`, you simply specify the key of another job or template from which the current job should inherit configurations.

```yaml
.template: &template
  script:
    - echo "This is a template."

job1:
  <<: *template
  stage: test

job2:
  extends: .template
  stage: deploy
  script:
    - echo "This job extends the template."
```

### Advantages of `extends`

- **DRY Configurations**: Reduces duplication by allowing jobs to inherit common configurations.
- **Flexibility**: Can extend multiple configurations, enabling complex setups with minimal duplication.
- **Maintainability**: Simplifies `.gitlab-ci.yml` by extracting common parts into reusable templates.

### Disadvantages of `extends`

- **Complexity**: Overuse can make the pipeline configuration hard to understand, especially with deep inheritance trees.
- **Debugging Difficulty**: Errors may be harder to trace back to the source when configurations are spread across multiple sections.

## The `!reference` Tag

The `!reference` tag is a YAML feature utilized by GitLab CI/CD to insert a reference to another part of the CI/CD configuration. It's particularly useful for reusing specific configurations like variables or scripts without inheriting an entire job's configuration.

### Advantages of `!reference`

- **Precision**: Allows for the exact parts of configurations to be reused, rather than inheriting entire jobs.
- **Reduce Duplication**: Similar to `extends`, it helps keep the CI/CD configuration DRY.
- **Flexibility**: Can be used to reference and reuse any part of the CI/CD configuration, not just entire jobs.

### Disadvantages of `!reference`

- **Limited to GitLab 13.9 and above**: Only available in GitLab versions 13.9 and later.
- **Potential for Confusion**: If overused or used without clear structure, it can make the pipeline configuration difficult to navigate.
- **Specificity**: While it allows for precise configuration sharing, it might require more setup for complete job configurations compared to `extends`.

## Full Example
Below you will find your pipeline modified to use `extends`. While the use here is minor, it could be expanded if our jobs shared more common code.

```yaml
---
variables:
  PROJECT_IMAGE: gitlab.home.arpa:5050/developer/workshop/project-image:latest
  GITLAB_CLI_IMAGE: gitlab.home.arpa:5050/developer/workshop/gitlab-release-cli:latest
  PROJECT_API: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}
  PACKAGE_REGISTRY_GENERIC_URL: ${PROJECT_API}/packages/generic
  PACKAGE_ARCH: generic
  PACKAGE_NAME: ${CI_PROJECT_NAME}-${PACKAGE_VERSION}-${PACKAGE_ARCH}

stages:
  - lint
  - build
  - analysis
  - test
  - package
  - release

workflow:
  rules:
    - if: $CI_COMMIT_TAG
      variables:
        PACKAGE_VERSION: "${CI_COMMIT_TAG}"
    - if: ($CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH)
      variables:
        PACKAGE_VERSION: "${CI_COMMIT_BRANCH}_${CI_COMMIT_SHORT_SHA}"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        PACKAGE_VERSION: "latest"

.base:
  image: $PROJECT_IMAGE

lint:
  extends: .base
  stage: lint
  script:
    - find . -iname '*.c' -o -iname '*.h' | xargs clang-format -i --style=microsoft
    - git diff --exit-code || (echo "Code style issues found. Please run 'clang-format' locally." && exit 1)

build:
  extends: .base
  stage: build
  needs: []
  script:
    - cmake -DPACKAGE_NAME=${CI_PROJECT_NAME} -DPACKAGE_VERSION=${PACKAGE_VERSION} -DPACKAGE_ARCH=${PACKAGE_ARCH} -S . -B build --preset default
    - cmake --build build
  artifacts:
    paths:
      - build/
    expire_in: 1 week

analysis:
  extends: .base
  stage: analysis
  needs: [build]
  script:
    - find . -iname '*.c' -o -iname '*.h' | xargs clang-tidy -p build --checks=*,-clang-analyzer-alpha.*

test:
  extends: .base
  stage: test
  needs: [build]
  script:
    - ./build/main > output.txt
    - cat output.txt
    - |
      if grep -q "Hello, GitLab CI/CD!" output.txt; then
        echo "Test Passed" > results.txt
      else
        echo "Test Failed" > results.txt
        exit 1
      fi
  artifacts:
    paths:
      - results.txt
    expire_in: 1 week

package:
  extends: .base
  stage: package
  script:
    - cd build
    - cpack
    - cp out/${PACKAGE_NAME}.tar.gz ..
  needs: [build, test]
  artifacts:
    paths:
      - ${PACKAGE_NAME}.tar.gz
    expire_in: 1 week

release:
  stage: release
  image: $GITLAB_CLI_IMAGE
  rules:
    - if: $CI_COMMIT_TAG =~ /^v?\d+\.\d+\.\d+$/
      variables:
        CHANGELOG_GET_COMMAND: 'curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X GET "${PROJECT_API}/repository/changelog?version=${CI_COMMIT_TAG}" | jq -r .notes > release_notes.md'
        CHANGELOG_POST_COMMAND: 'curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X POST "${PROJECT_API}/repository/changelog?version=${CI_COMMIT_TAG}"'
        REMOVE_TAG_COMMAND: 'true'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        CHANGELOG_GET_COMMAND: 'echo "This is the latest build for the project repository. It contains no changelog." > release_notes.md'
        CHANGELOG_POST_COMMAND: 'echo "No changelog update needed, this is the latest build..."'
        REMOVE_TAG_COMMAND: 'echo "Removing previous matching tag if found..." && curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X DELETE "${PROJECT_API}/repository/tags/${PACKAGE_VERSION}"'
  dependencies:
    - package
  script:
    - 'eval "${REMOVE_TAG_COMMAND}"'
    - echo "Creating changelog..."
    - 'eval "${CHANGELOG_GET_COMMAND}" && eval "${CHANGELOG_POST_COMMAND}"'
    - echo "Uploading packages..."
    - 'curl -H "JOB-TOKEN:${CI_JOB_TOKEN}" --upload-file "${PACKAGE_NAME}.tar.gz" "${PACKAGE_REGISTRY_GENERIC_URL}/${CI_PROJECT_NAME}/${PACKAGE_VERSION}/${PACKAGE_NAME}.tar.gz"'
    - echo "Creating release..."
  release:
    name: '$CI_PROJECT_NAME:$PACKAGE_VERSION'
    description: release_notes.md
    tag_name: '$PACKAGE_VERSION'
    ref: '$CI_COMMIT_SHA'
    assets:
      links:
        - name: '${PACKAGE_NAME}.tar.gz'
          url: "${PACKAGE_REGISTRY_GENERIC_URL}/${CI_PROJECT_NAME}/${PACKAGE_VERSION}/${PACKAGE_NAME}.tar.gz"
```

## Conclusion

Both `extends` and `!reference` are powerful features in GitLab CI/CD that promote reusability and maintainability of pipeline configurations. Choosing between them depends on the specific needs of your CI/CD setup:

- Use `extends` when you need to inherit entire job configurations or create templates for common job patterns.
- Use `!reference` for more granular control over what parts of the configuration to reuse, such as variables or specific script commands.

Incorporating these features effectively can greatly streamline your CI/CD pipeline setup, but it's important to use them judiciously to maintain clarity and manageability of your `.gitlab-ci.yml`.

[Overview](../../README.md) | [Back](./01_using_needs_keyword.md) | [Next](./03_using_include.md)
