# Creating Dynamic Pipelines

Creating dynamic pipelines in GitLab using Jsonnet can streamline and simplify your CI/CD process, especially for complex workflows. Jsonnet is a data templating language that allows you to generate JSON or YAML configurations dynamically. This tutorial will guide you through setting up dynamic GitLab pipelines with Jsonnet.

## Install Jsonnet
First, you need to have Jsonnet installed on your machine. You can download and install Jsonnet from its official GitHub repository.

```bash
sudo apt-get install jsonnet
```

## Create a Jsonnet Template
Create a `.gitlab-ci.jsonnet` file in your repository's root directory. This file will define your pipeline configuration.

Example `.gitlab-ci.jsonnet`:

```json
local job(name, script) = {
  [name]: {
    script: script,
  },
};

{
  stages: ['build', 'test', 'deploy'],
  
  build: job('build', ['echo "Building project..."']),
  
  test: job('test', ['echo "Running tests..."']),
  
  deploy: job('deploy', ['echo "Deploying application..."']),
}
```

This Jsonnet script defines a simple pipeline with three stages: `build`, `test`, and `deploy`. Each stage has one job that echoes a message.

## Generate GitLab CI Configuration
You need to convert the Jsonnet template into a `.gitlab-ci.yml` file that GitLab CI can understand. You can do this manually by running the Jsonnet command or automate it within your pipeline.

Manual generation command:

```bash
jsonnet .gitlab-ci.jsonnet -o .gitlab-ci.yml
```

## Automating Pipeline Generation

To automate the generation of `.main.yml` from `.gitlab-ci.jsonnet`, you can create a job in your GitLab CI configuration that runs before all other jobs.

Create a `.gitlab-ci.yml` with the following code:

```yaml
stages:
  - prepare
  - build
  - test
  - deploy

generate-pipeline:
  stage: prepare
  image: jsonnet/jsonnet
  script:
    - jsonnet .gitlab-ci.jsonnet -o main.yml
  artifacts:
    paths:
      - main.yml
```

This job uses a Docker image with Jsonnet pre-installed to generate the `main.yml` file and then saves it as an artifact.

## Using Jsonnet Libraries
Jsonnet allows you to modularize your configuration by using libraries. You can define common templates in separate files and import them into your main `.gitlab-ci.jsonnet` file.

Example library usage:

```json
Copy code
// common.jsonnet
{
  job(name, script): {
    [name]: {
      script: script,
    },
  },
}
```

Import and use in .gitlab-ci.jsonnet:


```json
local common = import 'common.jsonnet';

{
  stages: ['build', 'test', 'deploy'],
  
  build: common.job('build', ['echo "Building project..."']),
  
  test: common.job('test', ['echo "Running tests..."']),
  
  deploy: common.job('deploy', ['echo "Deploying application..."']),
}
```

## Applying Dynamic Pipelines to Your Repository
Now that you have an understanding of dynamic pipelines using jsonnet, let's apply this to a common use case in your repository. We will plan on building for multiple architectures. The steps for build multiple archs are generally the same minus differences in the toolchains, so we want to take advantage of dynamic pipeline generation so we only have to maintain one `job`.

### Create the jsonnet File
First, create the jsonnet file that will contain all of our dynamically generated jobs. Note that some of these are extending the base jobs, which we will modify later.

```json
local build(arch) =
{
  extends: '.build',
  variables: { PACKAGE_ARCH: arch }
};

local analysis(arch) =
{
  extends: '.analysis',
  needs: [['build:' + arch]],
  variables: { PACKAGE_ARCH: arch} 
};

local test(arch) =
{
  extends: '.test',
  needs: [['build:' + arch]],
  variables: { PACKAGE_ARCH: arch }
};

local package(arch) =
{
  extends: '.package',
  needs: [['build:' + arch], ['analysis:' + arch], ['test:' + arch]],
  variables: { PACKAGE_ARCH: arch }
};

local archs = ['x86_64', 'aarch64', 'mips', 'mipsel'];

{
  ['build:' + x]: build(x) for x in archs
}

{
  ['analysis:' + x]: analysis(x) for x in archs
}

{
  ['test:' + x]: test(x) for x in archs
}

{
  ['package:' + x]: package(x) for x in archs
}

```

### Modify the `.gitlab-ci.yml` File
We will need to add the generation step to our `.gitlab-ci.yml` file.

```yaml
---
variables:
  PROJECT_IMAGE: gitlab.home.arpa:5050/developer/workshop/project-image:latest

jsonnet:build:
  image: ${PROJECT_IMAGE}
  script:
    - jsonnet '.gitlab/ci/gitlab-ci.jsonnet' > generated-config.yml
    - cat generated-config.yml
  artifacts:
    paths:
      - generated-config.yml

trigger:pipeline:
  needs:
    - jsonnet:build
  trigger:
    include:
      - local: '.gitlab/ci/main.yml'      
      - artifact: generated-config.yml
        job: jsonnet:build
    strategy: depend
```

### Modify the YAML files to Template Jobs
We now convert many of the yaml files into templated jobs. This allows you to keep the shared functionality inside the yaml file and keep your jsonnet code simple and easy to maintain. Note that some names have changed, variables shifted, etc...

#### build.yml
```yaml
---
.build:
  extends: .base
  stage: build
  needs: []
  variables:
    PACKAGE_NAME: ${CI_PROJECT_NAME}-${PACKAGE_VERSION}-${PACKAGE_ARCH}
  script:
    - cmake -DPACKAGE_NAME=${CI_PROJECT_NAME} -DPACKAGE_VERSION=${PACKAGE_VERSION} -DPACKAGE_ARCH=${PACKAGE_ARCH} -S . -B build-${PACKAGE_ARCH} --preset default
    - cmake --build build-${PACKAGE_ARCH}
  artifacts:
    paths:
      - build-${PACKAGE_ARCH}
    expire_in: 1 week
```

#### analysis.yml
```yaml
---
.analysis:
  extends: .base
  stage: analysis
  script:
    - find . -iname '*.c' -o -iname '*.h' | xargs clang-tidy -p build-${PACKAGE_ARCH} --checks=*,-clang-analyzer-alpha.*
```

#### test.yml
```yaml
---
.test:
  extends: .base
  stage: test
  needs: [build]
  variables:
    PACKAGE_NAME: ${CI_PROJECT_NAME}-${PACKAGE_VERSION}-${PACKAGE_ARCH}
  script:
    - ./build-${PACKAGE_ARCH}/main > output.txt
    - echo ${PACKAGE_ARCH} >> output.txt
    - cat output.txt
    - |
      if grep -q "Hello, GitLab CI/CD!" output.txt && grep -q ${PACKAGE_ARCH} output.txt; then
        echo "Test Passed" > results.txt
      else
        echo "Test Failed" > results.txt
        exit 1
      fi
  artifacts:
    paths:
      - results.txt
    expire_in: 1 week
```

#### package.yml
```yaml
---
.package:
  extends: .base
  stage: package
  variables:
    PACKAGE_NAME: ${CI_PROJECT_NAME}-${PACKAGE_VERSION}-${PACKAGE_ARCH}
  script:
    - mkdir -p out
    - cd build-${PACKAGE_ARCH}
    - cpack
    - cp out/${PACKAGE_NAME}.tar.gz ../out
  artifacts:
    paths:
      - out/${PACKAGE_NAME}.tar.gz
    expire_in: 1 week
```

#### release.yml
Note that this file has had major changes to accomodate the new dynamic pipeline. A new job is introduced to consolidate all the different packages into one single package for release.

```yaml
---
.release:base:
  stage: release
  image: $GITLAB_CLI_IMAGE
  variables:
    PROJECT_API: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}
    PACKAGE_REGISTRY_GENERIC_URL: ${PROJECT_API}/packages/generic
    TAR_NAME: ${CI_PROJECT_NAME}-${PACKAGE_VERSION}.tar.gz
  rules:
    - if: $CI_COMMIT_TAG =~ /^v?\d+\.\d+\.\d+$/
      variables:
        CHANGELOG_GET_COMMAND: 'curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X GET "${PROJECT_API}/repository/changelog?version=${CI_COMMIT_TAG}" | jq -r .notes > release_notes.md'
        CHANGELOG_POST_COMMAND: 'curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X POST "${PROJECT_API}/repository/changelog?version=${CI_COMMIT_TAG}"'
        REMOVE_TAG_COMMAND: 'true'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        CHANGELOG_GET_COMMAND: 'echo "This is the latest build for the project repository. It contains no changelog." > release_notes.md'
        CHANGELOG_POST_COMMAND: 'echo "No changelog update needed, this is the latest build..."'
        REMOVE_TAG_COMMAND: 'echo "Removing previous matching tag if found..." && curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X DELETE "${PROJECT_API}/repository/tags/${PACKAGE_VERSION}"'

release:prepare:
  extends: .release:base
  script:
    - tar -cvf ${TAR_NAME} -C out/ .
  artifacts:
    paths:
      - ${TAR_NAME}
    expire_in: 1 week

release:
  extends: .release:base
  needs: [release:prepare]
  script:
    - 'eval "${REMOVE_TAG_COMMAND}"'
    - echo "Creating changelog..."
    - 'eval "${CHANGELOG_GET_COMMAND}" && eval "${CHANGELOG_POST_COMMAND}"'
    - echo "Uploading packages..."
    - 'curl -H "JOB-TOKEN:${CI_JOB_TOKEN}" --upload-file "${TAR_NAME}" "${PACKAGE_REGISTRY_GENERIC_URL}/${CI_PROJECT_NAME}/${PACKAGE_VERSION}/${TAR_NAME}"'
    - echo "Creating release..."
  release:
    name: '$CI_PROJECT_NAME:$PACKAGE_VERSION'
    description: release_notes.md
    tag_name: '$PACKAGE_VERSION'
    ref: '$CI_COMMIT_SHA'
    assets:
      links:
        - name: '${TAR_NAME}'
          url: "${PACKAGE_REGISTRY_GENERIC_URL}/${CI_PROJECT_NAME}/${PACKAGE_VERSION}/${TAR_NAME}"
```

Now your pipeline is setup to eventually build multiple architectures. You could, for example, include specific images that have the correct toolchains as a variable in the jsonnet. The possibilities are endless. 

>**__CAUTION__**: Dynamic pipelines open up infinite possibilities in GitLab CI/CD. However, there can be too much of a good thing. Be careful with your complexity and how difficult your pipeline becomes to maintain the more clever you try to get.

## Conclusion
Using Jsonnet for your GitLab CI pipelines allows you to dynamically generate configurations, making it easier to manage complex workflows and share common configurations across projects. Experiment with Jsonnet's features to get the most out of your CI/CD pipelines.

[Overview](../../README.md) | [Back](./05_downstream_and_mp_pipelines.md) | [Next](../04_advanced_pipelines/01_scheduled_pipelines.md)
