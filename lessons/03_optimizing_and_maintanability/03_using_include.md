# Using the `include` Keyword

The `include` keyword in GitLab CI/CD configuration files allows you to include external YAML files, either locally or from a remote repository, into your main CI/CD configuration. This feature helps to modularize and reuse configuration across different projects or pipelines, improving maintainability and reducing duplication.

[TOC]

## Syntax

```yaml
include:
  - local: path/to/local/file.yml
  - remote: https://example.com/path/to/remote/file.yml
  - project: 'group/project'
    file: path/to/file.yml
```

- `local`: Includes a YAML file from the local repository.
- `remote`: Includes a YAML file from a remote URL.
- `project`: Includes a YAML file from another project within the same GitLab instance.

## Usage

### Local Includes
You can include YAML files from the local repository by specifying the path to the file relative to the `.gitlab-ci.yml` file.

```yaml
include:
  - local: path/to/local/file.yml
```
### Remote Includes
include:
  - remote: https://example.com/path/to/remote/file.yml

You can include YAML files from a remote repository by specifying the URL of the file.
```yaml
include:
  - remote: https://example.com/path/to/remote/file.yml
```

### Project Includes
You can include YAML files from another project within the same GitLab instance by specifying the project path and the file path.

```yaml
include:
  - project: 'group/project'
    file: path/to/file.yml
```

## Benefits

- **Modularity**: Splitting configuration into smaller files allows for easier maintenance and readability.
- **Reusability**: Share common configuration across multiple projects or pipelines.
- **Version Control**: Included files are versioned along with the main configuration file, providing a clear history of changes.

## Considerations

- **Security**: Be cautious when including files from external sources, as they can introduce security risks. Always verify the source of included files.
- **Performance**: Excessive use of includes may impact pipeline performance, especially when fetching remote files. Consider caching or optimizing the usage of includes.

## `ref` Keyword

Additionally, the `ref` keyword can be used within the `include` section to specify a specific branch, tag, or commit from which to include the configuration file. This allows for precise control over which version of the included file is used in the pipeline.

```yaml
include:
  - remote: https://example.com/path/to/remote/file.yml
    ref: main
```

## Example

```yaml
include:
  - local: .gitlab/ci/common.yml
  - project: 'my-group/my-project'
    file: .gitlab/ci/shared.yml
    ref: main

stages:
  - build
  - test

# Job definitions...
```

In this example, we include a common configuration file from the local repository and a shared configuration file from another project within the same GitLab instance.

## Modifying Your Pipeline
Let's apply the include keyword to split our pipeline into smaller files for maintainability. As your different stages grow, it makes sense to separate your pipeline into multiple files. We will create one file per pipeline stage inside the `.gitlab/ci` folder and use the `include` keyword to include them on our main `.gitlab-ci.yml`:

### lint.yml
```yaml
---
lint:
  extends: .base
  stage: lint
  script:
    - find . -iname '*.c' -o -iname '*.h' | xargs clang-format -i --style=microsoft
    - git diff --exit-code || (echo "Code style issues found. Please run 'clang-format' locally." && exit 1)
```

### build.yml
```yaml
---
build:
  extends: .base
  stage: build
  needs: []
  script:
    - cmake -DPACKAGE_NAME=${CI_PROJECT_NAME} -DPACKAGE_VERSION=${PACKAGE_VERSION} -DPACKAGE_ARCH=${PACKAGE_ARCH} -S . -B build --preset default
    - cmake --build build
  artifacts:
    paths:
      - build/
    expire_in: 1 week
```

### analysis.yml
```yaml
---
analysis:
  extends: .base
  stage: analysis
  needs: [build]
  script:
    - find . -iname '*.c' -o -iname '*.h' | xargs clang-tidy -p build --checks=*,-clang-analyzer-alpha.*
```

### test.yml
```yaml
---
test:
  extends: .base
  stage: test
  needs: [build]
  script:
    - ./build/main > output.txt
    - cat output.txt
    - |
      if grep -q "Hello, GitLab CI/CD!" output.txt; then
        echo "Test Passed" > results.txt
      else
        echo "Test Failed" > results.txt
        exit 1
      fi
  artifacts:
    paths:
      - results.txt
    expire_in: 1 week
```

### package.yml
```yaml
---
package:
  extends: .base
  stage: package
  script:
    - cd build
    - cpack
    - cp out/${PACKAGE_NAME}.tar.gz ..
  needs: [build, test]
  artifacts:
    paths:
      - ${PACKAGE_NAME}.tar.gz
    expire_in: 1 week
```

### release.yml
```yaml
---
release:
  stage: release
  image: $GITLAB_CLI_IMAGE
  rules:
    - if: $CI_COMMIT_TAG =~ /^v?\d+\.\d+\.\d+$/
      variables:
        CHANGELOG_GET_COMMAND: 'curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X GET "${PROJECT_API}/repository/changelog?version=${CI_COMMIT_TAG}" | jq -r .notes > release_notes.md'
        CHANGELOG_POST_COMMAND: 'curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X POST "${PROJECT_API}/repository/changelog?version=${CI_COMMIT_TAG}"'
        REMOVE_TAG_COMMAND: 'true'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        CHANGELOG_GET_COMMAND: 'echo "This is the latest build for the project repository. It contains no changelog." > release_notes.md'
        CHANGELOG_POST_COMMAND: 'echo "No changelog update needed, this is the latest build..."'
        REMOVE_TAG_COMMAND: 'echo "Removing previous matching tag if found..." && curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X DELETE "${PROJECT_API}/repository/tags/${PACKAGE_VERSION}"'
  dependencies:
    - package
  script:
    - 'eval "${REMOVE_TAG_COMMAND}"'
    - echo "Creating changelog..."
    - 'eval "${CHANGELOG_GET_COMMAND}" && eval "${CHANGELOG_POST_COMMAND}"'
    - echo "Uploading packages..."
    - 'curl -H "JOB-TOKEN:${CI_JOB_TOKEN}" --upload-file "${PACKAGE_NAME}.tar.gz" "${PACKAGE_REGISTRY_GENERIC_URL}/${CI_PROJECT_NAME}/${PACKAGE_VERSION}/${PACKAGE_NAME}.tar.gz"'
    - echo "Creating release..."
  release:
    name: '$CI_PROJECT_NAME:$PACKAGE_VERSION'
    description: release_notes.md
    tag_name: '$PACKAGE_VERSION'
    ref: '$CI_COMMIT_SHA'
    assets:
      links:
        - name: '${PACKAGE_NAME}.tar.gz'
          url: "${PACKAGE_REGISTRY_GENERIC_URL}/${CI_PROJECT_NAME}/${PACKAGE_VERSION}/${PACKAGE_NAME}.tar.gz"
```

## Conclusion
The include keyword in GitLab CI/CD configuration files offers a powerful mechanism for modularizing, reusing, and organizing CI/CD configuration. By allowing the inclusion of external YAML files from local, remote, or other project sources, GitLab facilitates the creation of more maintainable, scalable, and efficient CI/CD pipelines. However, it's essential to exercise caution regarding security implications when including files from external sources and to be mindful of potential performance impacts, particularly with remote file retrieval. Overall, leveraging the include keyword enhances the development workflow by promoting code reuse, simplifying configuration management, and ultimately contributing to a more streamlined and robust CI/CD process.

[Overview](../../README.md) | [Back](./02_using_extends_and_reference.md) | [Next](./04_using_parallel.md)
