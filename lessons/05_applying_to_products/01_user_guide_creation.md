# Creating a User Guide from README.md

Creating a user guide in PDF format from a Markdown file is a common task for developers and technical writers. This tutorial will guide you through the process of converting a Markdown document into a polished PDF user guide using Pandoc and LaTeX. Pandoc is a universal document converter that can read and write numerous file formats, while LaTeX is a high-quality typesetting system; together, they enable the creation of professionally formatted documents.

## Step 1: Prepare Your Markdown Document

Ensure your Markdown document is ready and contains all the sections you want in your user guide. Markdown allows you to structure your document with headings, lists, images, links, and formatted text, which can be directly converted into PDF format.

## Step 2: Install Pandoc and LaTeX

Follow the installation instructions for Pandoc and your chosen LaTeX distribution. Installation processes may vary depending on your operating system. For the purposes of this workshop, you can use the provided Docker image.

## Step 3: Convert Markdown to PDF

Open a terminal or command prompt and navigate to the directory containing your Markdown file. Use Pandoc to convert your Markdown document to a PDF file. Replace `yourfile.md` with the name of your Markdown file.

```bash
pandoc yourfile.md -o userguide.pdf
```

## Step 4: Customize the PDF Output

Pandoc allows for extensive customization of the PDF output through command-line options or a separate Pandoc template file. You can specify a PDF engine, adjust document margins, or include a table of contents among other customizations.

```bash
pandoc yourfile.md -o userguide.pdf
```

## Step 5: Review and Edit

Review your PDF document. You may need to go back and adjust your Markdown document or the Pandoc command options to achieve the desired formatting and layout.

## Additional Tips

- **Images**: If your Markdown document includes images, ensure they are accessible at the paths specified in your document during conversion.
- **Metadata**: You can include metadata such as the title, author, and date at the beginning of your Markdown file in a YAML block for better document control.
- **LaTeX Templates**: For more complex formatting, consider creating a custom LaTeX template and specifying it with Pandoc's `--template` option.

## Modify Your Pipeline to Build a User Guide

1. Create a new component called `create-user-guide` in your `components` project with the following content:

#### template.yml
```yaml
spec:
  inputs:
    image:
      default: ${CI_REGISTRY}/${CI_PROJECT_PATH}/pandoc:latest
    stage:
      default: docs
    file:
      default: "README.md"
    name:
      default: "user_guide"
---

docs:user-guide:
  image: $[[ inputs.image ]]
  stage: $[[ inputs.stage ]]
  needs: []
  script:
    - mkdir -p docs/
    - pandoc $[[ inputs.file ]] --pdf-engine=xelatex -V geometry:margin=1in -V fontsize=12pt --toc -o docs/$[[ inputs.name ]].pdf
  artifacts:
    paths:
      - docs/
    expire_in: 1 week
```

Publish the component by tagging a new release.

2. Modify your `main.yml` to use the new component:

```yaml
---
variables:
  PROJECT_IMAGE: ${CI_REGISTRY}/${CI_PROJECT_PATH}/project-image:latest
  GITLAB_CLI_IMAGE: ${CI_REGISTRY}/${CI_PROJECT_PATH}/gitlab-release-cli:latest
  PYTHON_PACKAGE_NAME: 'python-package'
  PYTHON_PACKAGE_VERSION: '0.0.0'

stages:
  - docs
  - lint
  - build
  - analysis
  - test
  - package
  - release

workflow:
  rules:
    - if: $CI_COMMIT_TAG
      variables:
        PACKAGE_VERSION: '${CI_COMMIT_TAG}'
    - if: ($CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH)
      variables:
        PACKAGE_VERSION: "${CI_COMMIT_BRANCH}_${CI_COMMIT_SHORT_SHA}"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        PACKAGE_VERSION: "latest"
    - if: $CI_COMMIT_TAG =~ /^(\d+\.)?(\d+\.)?(\*|\d+)$/
      variables:
        PYTHON_PACKAGE_VERSION: '${CI_COMMIT_TAG}'

.base:
  image: $PROJECT_IMAGE

include:
  - component: gitlab.home.arpa/developer/components/clang-format-linting@1.0.0
    inputs:
      stage: lint
      find_directory: ./src
  - component: gitlab.home.arpa/developer/components/clang-tidy-analysis@1.0.0
    inputs:
      stage: analysis
      find_directory: ./src
      ctidy_database: build-${PACKAGE_ARCH}
  - component: gitlab.home.arpa/developer/components/create-user-guide@1.0.0
    inputs:
      stage: docs
      file: ./README.md
      name: ${CI_PROJECT_NAME}_user_guide
  - local: .gitlab/ci/build.yml
  - local: .gitlab/ci/test.yml
  - local: .gitlab/ci/package.yml
  - local: .gitlab/ci/release.yml
```

3. Modify your `release.yml` to include the documentation in the packaging and release:

```yaml
---
.release:base:
  stage: release
  image: $GITLAB_CLI_IMAGE
  variables:
    TAR_NAME: ${CI_PROJECT_NAME}-${PACKAGE_VERSION}.tar.gz
    TAR_DOCS_NAME: ${CI_PROJECT_NAME}-userguide-${PACKAGE_VERSION}.tar.gz

package:all:
  extends: .release:base
  script:
    - tar -cvf ${TAR_NAME} -C out/ .
    - tar -cvf ${TAR_DOCS_NAME} -C docs/ .
  artifacts:
    paths:
      - ${TAR_NAME}
      - ${TAR_DOCS_NAME}
      - out/dist
    expire_in: 1 week

release:
  extends: .release:base
  rules:
    - if: $CI_COMMIT_TAG =~ /^(\d+\.)?(\d+\.)?(\*|\d+)$/
      variables:
        CHANGELOG_GET_COMMAND: 'curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X GET "${PROJECT_API}/repository/changelog?version=${CI_COMMIT_TAG}" | jq -r .notes > release_notes.md'
        CHANGELOG_POST_COMMAND: 'curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X POST "${PROJECT_API}/repository/changelog?version=${CI_COMMIT_TAG}"'
        REMOVE_TAG_COMMAND: 'true'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        CHANGELOG_GET_COMMAND: 'echo "This is the latest build for the project repository. It contains no changelog." > release_notes.md'
        CHANGELOG_POST_COMMAND: 'echo "No changelog update needed, this is the latest build..."'
        REMOVE_TAG_COMMAND: 'echo "Removing previous matching tag if found..." && curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X DELETE "${PROJECT_API}/repository/tags/${PACKAGE_VERSION}"'
  needs: [package:all]
  variables:
    PROJECT_API: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}
    PACKAGE_REGISTRY_GENERIC_URL: ${PROJECT_API}/packages/generic
  script:
    - eval "${REMOVE_TAG_COMMAND}"
    - |
      echo "Creating changelog..."
      eval "${CHANGELOG_GET_COMMAND}" && eval "${CHANGELOG_POST_COMMAND}"
    - | 
      echo "Uploading generic packages..."
      curl -H "JOB-TOKEN:${CI_JOB_TOKEN}" --upload-file "${TAR_NAME}" "${PACKAGE_REGISTRY_GENERIC_URL}/${CI_PROJECT_NAME}/${PACKAGE_VERSION}/${TAR_NAME}"
      curl -H "JOB-TOKEN:${CI_JOB_TOKEN}" --upload-file "${TAR_DOCS_NAME}" "${PACKAGE_REGISTRY_GENERIC_URL}/${CI_PROJECT_NAME}/${PACKAGE_VERSION}/${TAR_DOCS_NAME}"
    - |
      echo "Deleting old Python packages..."
      curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X GET "${PROJECT_API}/packages" -o packages.json
      eval "jq '.[] | select(.name==\"${PYTHON_PACKAGE_NAME}\" and .version==\"${PYTHON_PACKAGE_VERSION}\") | .id' packages.json | tee pkgid"
      export PACKAGE_ID=$(cat pkgid)
      curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X DELETE "${PROJECT_API}/packages/$PACKAGE_ID"
    - |
      echo "Publishing Python packages..."
      TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token twine upload --repository-url ${PROJECT_API}/packages/pypi out/dist/*.whl
    - echo "Creating release..."
  release:
    name: '$CI_PROJECT_NAME:$PACKAGE_VERSION'
    description: release_notes.md
    tag_name: '$PACKAGE_VERSION'
    ref: '$CI_COMMIT_SHA'
    assets:
      links:
        - name: '${TAR_NAME}'
          url: "${PACKAGE_REGISTRY_GENERIC_URL}/${CI_PROJECT_NAME}/${PACKAGE_VERSION}/${TAR_NAME}"
        - name: '${TAR_DOCS_NAME}'
          url: "${PACKAGE_REGISTRY_GENERIC_URL}/${CI_PROJECT_NAME}/${PACKAGE_VERSION}/${TAR_DOCS_NAME}"
```

Your project should now auto-build the user guide into a PDF from your README.md.

## Conclusion

Converting a Markdown document to a PDF user guide with Pandoc and LaTeX offers a flexible and powerful way to create professional-quality documents. With the basic process outlined in this tutorial, you can start with simple conversions and gradually explore more advanced formatting and customization options to suit your needs.

[Overview](../../README.md) | [Back](../04_advanced_pipelines/05_deploy_environments.md) | [Next](./02_requirements_testing.md)
