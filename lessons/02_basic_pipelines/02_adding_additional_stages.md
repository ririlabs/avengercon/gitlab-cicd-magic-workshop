# Adding Additional Stages to Your Pipeline

In this guide, we will discuss how to enhance your pipeline by adding linting and static analysis stages using **clang-format** and **clang-tidy** respectively.

[TOC]

## Prerequisites

Before proceeding, ensure the following prerequisites are met:

- Your project is hosted on GitLab.
- You have a GitLab CI/CD pipeline configured.
- The project contains C/C++ source files.

## Adding Linting Stage using clang-format

**clang-format** is a tool used to enforce a consistent coding style across your project.

### Step 1: Update your `.gitlab-ci.yml` file

Add a new stage named `lint` to your `.gitlab-ci.yml` file. This stage will run **clang-format** to check the code style.

```yaml
---
---
stages:
  - lint
  - build
  - test

lint:
  image: gitlab.home.arpa:5050/developer/workshop/project-image:latest
  stage: lint
  script:
    - find . -iname '*.c' -o -iname '*.h' | xargs clang-format -i --style=microsoft
    - git diff --exit-code || (echo "Code style issues found. Please run 'clang-format' locally." && exit 1)

build:
  image: gitlab.home.arpa:5050/developer/workshop/project-image:latest
  stage: build
  script:
    - cmake -S . -B build
    - cmake --build build

test:
  image: gitlab.home.arpa:5050/developer/workshop/project-image:latest
  stage: test
  script:
    - ./build/main > output.txt
    - cat output.txt
    - |
      if grep -q "Hello, GitLab CI/CD!" output.txt; then
        echo "Test Passed"
      else
        echo "Test Failed"
        exit 1
      fi
```

In this configuration:

- The `lint` stage is added after the `build` stage.
- The `clang-format -i` command formats all C source files in the project.

### Step 2: Commit and Push Changes

Commit your changes to the `.gitlab-ci.yml` file and push them to your GitLab repository.

### Step 3: Verify

Trigger a pipeline run in GitLab to verify that the linting stage runs successfully.

## Adding Static Analysis Stage using clang-tidy

**clang-tidy** is a tool for static code analysis that checks for potential issues in your code.

### Step 1: Update your `.gitlab-ci.yml` file

Add a new stage named `analysis` to your `.gitlab-ci.yml` file. This stage will run **clang-tidy** to perform static analysis.

```yaml
---
stages:
  - lint
  - build
  - analysis
  - test

lint:
  image: gitlab.home.arpa:5050/developer/workshop/project-image:latest
  stage: lint
  script:
    - find . -iname '*.c' -o -iname '*.h' | xargs clang-format -i --style=microsoft
    - git diff --exit-code || (echo "Code style issues found. Please run 'clang-format' locally." && exit 1)

build:
  image: gitlab.home.arpa:5050/developer/workshop/project-image:latest
  stage: build
  script:
    - cmake -S . -B build
    - cmake --build build

analysis:
  image: gitlab.home.arpa:5050/developer/workshop/project-image:latest
  stage: analysis
  script:
    - find . -iname '*.c' -o -iname '*.h' | xargs clang-tidy -p build --checks=*,-clang-analyzer-alpha.*

test:
  image: gitlab.home.arpa:5050/developer/workshop/project-image:latest
  stage: test
  script:
    - ./build/main > output.txt
    - cat output.txt
    - |
      if grep -q "Hello, GitLab CI/CD!" output.txt; then
        echo "Test Passed"
      else
        echo "Test Failed"
        exit 1
      fi
```

In this configuration:

- The `analysis` stage is added after the `lint` stage.
- The `clang-tidy` command performs static analysis on all C source files in the project.

### Step 2: Commit and Push Changes

Commit your changes to the `.gitlab-ci.yml` file and push them to your GitLab repository.

### Step 3: Verify

Trigger a pipeline run in GitLab to verify that the static analysis stage runs successfully.

## Conclusion

Congratulations! You have successfully added linting and static analysis stages to your GitLab CI/CD pipeline using **clang-format** and **clang-tidy** respectively. These stages will help maintain code quality and catch potential issues early in the development process.

[Overview](../../README.md) | [Back](./01_your_first_pipeline.md) | [Next](./03_working_with_artifacts.md)
