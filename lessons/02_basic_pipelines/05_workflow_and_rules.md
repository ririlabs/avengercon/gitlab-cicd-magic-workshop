# Using the workflow and rules Keywords
This lesson aims to provide a clear understanding of the workflow, rules, and if keywords in GitLab CI/CD, helping you to efficiently manage and control your pipelines.

[TOC]

## Introduction
In GitLab CI/CD, pipelines are configured using a `.gitlab-ci.yml` file in the root of your repository. This file defines the structure and order of jobs that the GitLab Runner should execute. The `workflow`, `rules`, and `if keywords play a crucial role in controlling when and how jobs and pipelines are triggered.

## Workflow Keyword
The `workflow` keyword allows you to define conditions for when a pipeline should be created. It is a top-level configuration that determines whether or not to proceed with pipeline creation based on the specified criteria.

### Syntax
```yaml

workflow:
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
```

### Use Cases
- Liniting pipeline execution to specific branches.
- Running pipelines only for merge requests.

## Rules Keyword
The `rules` keyword controls the job execution based on specified conditions. It can be used at the job level or within the `workflow` keyword to make decisions based on variables, changes, or other criteria.

### Syntax
```yaml
job_name:
  script: echo "Hello, World!"
  rules:
    - if: '$CI_COMMIT_BRANCH == "develop"'
      when: manual
    - changes:
        - path/to/file
      when: always
```

### Use Cases
- Conditionally running jobs based on branch names.
- Executing jobs only when specific files are changed.

## If Keyword
The `if` keyword is used within `rules` to evaluate expressions that determine whether a job should be included or excluded from the pipeline.

### Syntax
```yaml
rules:
  - if: '$CI_COMMIT_TAG'
    when: always
  - if: '$CI_COMMIT_BRANCH == "feature/*"'
```

### Use Cases
- Running jobs for tagged commits.
- Filtering jobs for feature branches.

## Examples
Here are some examples demonstrating how to use the `workflow`, `rules`, and `if` keywords in your `.gitlab-ci.yml` file.

### Example 1: Workflow Restriction
```yaml
workflow:
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
```
This example restricts pipeline creation to commits pushed to the `main` branch.

### Example 2: Conditional Job Execution
```yaml
deploy_to_production:
  stage: deploy
  script: deploy_script.sh
  rules:
    - if: '$CI_COMMIT_BRANCH == "main" && $CI_PIPELINE_SOURCE == "push"'
      when: manual
    - if: '$CI_COMMIT_BRANCH == "main" && $CI_PIPELINE_SOURCE == "schedule"'
```

This job runs manually for pushes to the main branch and automatically for scheduled pipelines.

## Best Practices
- Use the `workflow` keyword to globally control pipeline creation.
- Utilize `rules` for fine-grained control over job execution.
- Employ the `if` keyword within rules for dynamic and conditional logic.
- Keep your `.gitlab-ci.yml` file clean and readable by grouping related jobs and using comments.

## Conclusion
Understanding and effectively using the `workflow`, `rules`, and `if` keywords in GitLab CI/CD can significantly enhance your automation processes, making your CI/CD pipelines more efficient and tailored to your project's needs.

[Overview](../../README.md) | [Back](./04_yaml_variables_anchors_references.md) | [Next](./06_adding_packaging_stage.md)
