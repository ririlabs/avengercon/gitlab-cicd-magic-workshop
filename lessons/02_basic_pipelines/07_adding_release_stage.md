# Adding a Release Stage

Following our earlier tutorial on enhancing your GitLab CI/CD pipeline with workflow controls and a detailed packaging process, we now introduce a `release` stage. This stage is crucial for creating a release on GitLab, managing changelog documentation, and uploading the packaged application to a registry.

[TOC]

## Introduction to the Release Stage

The `release` stage automates the creation of releases in your GitLab project, handling the generation or retrieval of changelogs, and uploading the final artifacts to your project's package registry. It's an essential step for maintaining accurate project documentation and ensuring that your compiled artifacts are readily available for deployment. We will explain the purpose of each keyword and the script commands executed during this stage.

## The GitLab release-cli Tool

GitLab's release-cli is a command-line tool designed to facilitate the creation and management of releases in a GitLab project. It's part of GitLab's continuous integration (CI) and continuous delivery (CD) solutions, allowing developers to automate the release process as part of their CI/CD pipelines. This tool is specifically created to interact with GitLab's Releases API, enabling users to create, update, and manage releases directly from the command line or within CI/CD pipeline scripts.

### Key Features of release-cli
- **Release Creation and Management:** Enables the creation of new releases or the update of existing ones. Users can specify release notes, associate milestones, and attach assets such as binaries or source code archives.

- **Asset Management:** Allows for the uploading of assets associated with a release. These can include binaries, documentation, or any other relevant files. Assets can be linked with a release, making it easier for users to find and download them.

- **Automation within CI/CD Pipelines:** Can be used within GitLab CI/CD pipelines to automate the release process. For example, when a pipeline runs successfully, release-cli can be triggered to create a new release, ensuring that the process is seamless and does not require manual intervention.

- **Integration with GitLab's API:** Leverages GitLab's Releases API, ensuring that actions performed with release-cli are reflected in the GitLab UI and vice versa. This integration provides a consistent experience whether releases are managed through the UI or the CLI.

### Getting Started with release-cli
To use release-cli, you would typically follow these steps:

- **Installation:** First, ensure that release-cli is installed. GitLab provides installation instructions in its documentation.

- **Configuration:** Configure your local environment or CI/CD pipeline to authenticate with GitLab. This usually involves setting up an access token with the appropriate permissions to create and manage releases.

- **Usage:** Use release-cli commands to manage releases. This can involve creating a new release, updating an existing one, uploading assets, or listing releases for a project.

- **Integration into CI/CD:** To automate release management, integrate release-cli commands into your GitLab CI/CD pipeline definitions. This way, releases can be created automatically based on certain triggers, such as a push to a specific branch or a successful pipeline run.

### Example Command
Here's a simple example of creating a release using release-cli:

```shell
release-cli create --name "Release v1.0" --description "Release description here" --tag-name "v1.0" --ref "main"
```

This command creates a new release named "Release v1.0" with the specified description, associated with the tag "v1.0" on the "main" branch.

### Summary
release-cli is a powerful tool for automating release management in GitLab projects, offering a wide range of capabilities for creating, updating, and managing releases. By integrating release-cli into CI/CD pipelines, teams can streamline their release processes, reduce manual effort, and ensure consistent delivery of their software products.

## A Complete Release Stage Example
The following is a complete `release` stage example for your pipeline. Note that we use `keywords` instead of the `release-cli` command arguments. GitLab knows how to convert these for the `release-cli` tool:

```yaml
release:
  stage: release
  image: $GITLAB_CLI_IMAGE
  rules:
    - if: $CI_COMMIT_TAG =~ /^v?\d+\.\d+\.\d+$/
      variables:
        CHANGELOG_GET_COMMAND: 'curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X GET "${PROJECT_API}/repository/changelog?version=${CI_COMMIT_TAG}" | jq -r .notes > release_notes.md'
        CHANGELOG_POST_COMMAND: 'curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X POST "${PROJECT_API}/repository/changelog?version=${CI_COMMIT_TAG}"'
        REMOVE_TAG_COMMAND: 'true'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        CHANGELOG_GET_COMMAND: 'echo "This is the latest build for the project repository. It contains no changelog." > release_notes.md'
        CHANGELOG_POST_COMMAND: 'echo "No changelog update needed, this is the latest build..."'
        REMOVE_TAG_COMMAND: 'echo "Removing previous matching tag if found..." && curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X DELETE "${PROJECT_API}/repository/tags/${PACKAGE_VERSION}"'
  dependencies:
    - package
  script:
    - 'eval "${REMOVE_TAG_COMMAND}"'
    - echo "Creating changelog..."
    - 'eval "${CHANGELOG_GET_COMMAND}" && eval "${CHANGELOG_POST_COMMAND}"'
    - echo "Uploading packages..."
    - 'curl -H "JOB-TOKEN:${CI_JOB_TOKEN}" --upload-file "${PACKAGE_NAME}.tar.gz" "${PACKAGE_REGISTRY_GENERIC_URL}/${CI_PROJECT_NAME}/${PACKAGE_VERSION}/${PACKAGE_NAME}.tar.gz"'
    - echo "Creating release..."
  release:
    name: '$CI_PROJECT_NAME:$PACKAGE_VERSION'
    description: release_notes.md
    tag_name: '$PACKAGE_VERSION'
    ref: '$CI_COMMIT_SHA'
    assets:
      links:
        - name: '${PACKAGE_NAME}.tar.gz'
          url: "${PACKAGE_REGISTRY_GENERIC_URL}/${CI_PROJECT_NAME}/${PACKAGE_VERSION}/${PACKAGE_NAME}.tar.gz"
```

The `release` stage is configured with several keywords and script commands, each serving a specific function:

### Keywords Explained

- **`stage: release`**: This assigns the job to the `release` stage of your pipeline, which typically runs after the `package` stage.
- **`image: $GITLAB_CLI_IMAGE`**: Specifies the Docker image to use for the job. This image should have the necessary tools (e.g., GitLab CLI, `curl`, `jq`) for interacting with GitLab's API and handling file uploads.
- **`rules`**: Determines when the job should run, based on conditions such as the presence of a commit tag or commits to the default branch.
  - **`if: $CI_COMMIT_TAG =~ /^v?\d+\.\d+\.\d+$/`**: This rule applies to commits tagged with a semantic versioning format, optionally starting with `v`. It's used for versioned releases.
  - **`if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH`**: This rule applies to commits on the default branch, typically used for the latest or continuous builds.

### Script Commands Detailed

- **`'eval "${REMOVE_TAG_COMMAND}"'`**: Executes the command stored in `REMOVE_TAG_COMMAND`. This can be a no-operation (`true`) for tagged commits or a command to remove a previous tag for default branch commits, ensuring that the latest build always reflects the current state of the default branch.
- **Changelog Management**:
  - **`'eval "${CHANGELOG_GET_COMMAND}"'`**: Executes the command to retrieve or generate the changelog. For versioned releases, it might fetch the changelog from the project's repository. For the default branch, it generates a placeholder message.
  - **`'eval "${CHANGELOG_POST_COMMAND}"'`**: Optionally posts the changelog back to the GitLab repository. This could be useful for updating project documentation or release notes.
- **Artifact Upload**:
  - **`'curl -H "JOB-TOKEN:${CI_JOB_TOKEN}" --upload-file "${PACKAGE_NAME}.tar.gz" "${PACKAGE_REGISTRY_GENERIC_URL}/${CI_PROJECT_NAME}/${PACKAGE_VERSION}/${PACKAGE_NAME}.tar.gz"'`**: Uploads the packaged artifact to GitLab's package registry using a job token for authentication. This command makes the artifact available for download or deployment.

### Release Keyword Configuration

- **`release`**:
  - **`name: '$CI_PROJECT_NAME:$PACKAGE_VERSION'`**: Sets the name of the release, incorporating the project name and package version for clarity.
  - **`description: release_notes.md`**: Points to the `release_notes.md` file as the source for the release description, which is displayed in the GitLab UI.
  - **`tag_name: '$PACKAGE_VERSION'`**: Specifies the tag name for the release, aligning it with the package version to maintain consistency.
  - **`ref: '$CI_COMMIT_SHA'`**: Associates the release with a specific commit SHA, ensuring traceability back to the source code at the time of the release.
  - **`assets`**:
    - **`links`**:
      - **`name: '${PACKAGE_NAME}.tar.gz'`**: Names the linked asset for clarity in the GitLab UI.
      - **`url: "${PACKAGE_REGISTRY_GENERIC_URL}/${CI_PROJECT_NAME}/${PACKAGE_VERSION}/${PACKAGE_NAME}.tar.gz"`**: Provides the URL to the uploaded artifact in the GitLab package registry, making it accessible to users and deployment processes.

### Full Example
Your `.gitlab-ci.yml` should now look like the following:

```yaml
---
variables:
  PROJECT_IMAGE: gitlab.home.arpa:5050/developer/workshop/project-image:latest
  GITLAB_CLI_IMAGE: gitlab.home.arpa:5050/developer/workshop/gitlab-release-cli:latest
  PROJECT_API: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}
  PACKAGE_REGISTRY_GENERIC_URL: ${PROJECT_API}/packages/generic
  PACKAGE_ARCH: generic
  PACKAGE_NAME: ${CI_PROJECT_NAME}-${PACKAGE_VERSION}-${PACKAGE_ARCH}

stages:
  - lint
  - build
  - analysis
  - test
  - package
  - release

workflow:
  rules:
    - if: $CI_COMMIT_TAG
      variables:
        PACKAGE_VERSION: "${CI_COMMIT_TAG}"
    - if: ($CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH)
      variables:
        PACKAGE_VERSION: "${CI_COMMIT_BRANCH}_${CI_COMMIT_SHORT_SHA}"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        PACKAGE_VERSION: "latest"

lint:
  image: $PROJECT_IMAGE
  stage: lint
  script:
    - find . -iname '*.c' -o -iname '*.h' | xargs clang-format -i --style=microsoft
    - git diff --exit-code || (echo "Code style issues found. Please run 'clang-format' locally." && exit 1)

build:
  image: $PROJECT_IMAGE
  stage: build
  script:
    - cmake -DPACKAGE_NAME=${CI_PROJECT_NAME} -DPACKAGE_VERSION=${PACKAGE_VERSION} -DPACKAGE_ARCH=${PACKAGE_ARCH} -S . -B build --preset default
    - cmake --build build
  artifacts:
    paths:
      - build/
    expire_in: 1 week

analysis:
  image: $PROJECT_IMAGE
  stage: analysis
  script:
    - find . -iname '*.c' -o -iname '*.h' | xargs clang-tidy -p build --checks=*,-clang-analyzer-alpha.*

test:
  image: $PROJECT_IMAGE
  stage: test
  script:
    - ./build/main > output.txt
    - cat output.txt
    - |
      if grep -q "Hello, GitLab CI/CD!" output.txt; then
        echo "Test Passed" > results.txt
      else
        echo "Test Failed" > results.txt
        exit 1
      fi
  artifacts:
    paths:
      - results.txt
    expire_in: 1 week

package:
  image: $PROJECT_IMAGE
  stage: package
  script:
    - cd build
    - cpack
    - cp out/${PACKAGE_NAME}.tar.gz ..
  dependencies:
    - build
    - test
  artifacts:
    paths:
      - ${PACKAGE_NAME}.tar.gz

release:
  stage: release
  image: $GITLAB_CLI_IMAGE
  rules:
    - if: $CI_COMMIT_TAG =~ /^v?\d+\.\d+\.\d+$/
      variables:
        CHANGELOG_GET_COMMAND: 'curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X GET "${PROJECT_API}/repository/changelog?version=${CI_COMMIT_TAG}" | jq -r .notes > release_notes.md'
        CHANGELOG_POST_COMMAND: 'curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X POST "${PROJECT_API}/repository/changelog?version=${CI_COMMIT_TAG}"'
        REMOVE_TAG_COMMAND: 'true'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        CHANGELOG_GET_COMMAND: 'echo "This is the latest build for the project repository. It contains no changelog." > release_notes.md'
        CHANGELOG_POST_COMMAND: 'echo "No changelog update needed, this is the latest build..."'
        REMOVE_TAG_COMMAND: 'echo "Removing previous matching tag if found..." && curl -H "PRIVATE-TOKEN:${PROJECT_API_TOKEN}" -X DELETE "${PROJECT_API}/repository/tags/${PACKAGE_VERSION}"'
  dependencies:
    - package
  script:
    - 'eval "${REMOVE_TAG_COMMAND}"'
    - echo "Creating changelog..."
    - 'eval "${CHANGELOG_GET_COMMAND}" && eval "${CHANGELOG_POST_COMMAND}"'
    - echo "Uploading packages..."
    - 'curl -H "JOB-TOKEN:${CI_JOB_TOKEN}" --upload-file "${PACKAGE_NAME}.tar.gz" "${PACKAGE_REGISTRY_GENERIC_URL}/${CI_PROJECT_NAME}/${PACKAGE_VERSION}/${PACKAGE_NAME}.tar.gz"'
    - echo "Creating release..."
  release:
    name: '$CI_PROJECT_NAME:$PACKAGE_VERSION'
    description: release_notes.md
    tag_name: '$PACKAGE_VERSION'
    ref: '$CI_COMMIT_SHA'
    assets:
      links:
        - name: '${PACKAGE_NAME}.tar.gz'
          url: "${PACKAGE_REGISTRY_GENERIC_URL}/${CI_PROJECT_NAME}/${PACKAGE_VERSION}/${PACKAGE_NAME}.tar.gz"
```

## Conclusion

The `release` stage in your GitLab CI/CD pipeline plays a crucial role in automating the release process, from managing changelogs to uploading artifacts and creating a visible, traceable release within GitLab. By understanding the purpose and function of each keyword and command, you can customize this stage to fit your project's specific needs, enhancing your CI/CD workflow's efficiency and effectiveness.

[Overview](../../README.md) | [Back](./06_adding_packaging_stage.md) | [Next](../03_optimizing_and_maintanability/01_using_needs_keyword.md)
