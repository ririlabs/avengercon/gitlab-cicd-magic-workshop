# Understanding YAML Variables, References, and Anchors in GitLab CI/CD

This document aims to provide a comprehensive overview of how to use and configure YAML variables, references, and anchors in your GitLab CI/CD pipelines. They are a key component in controlling the behavior of your CI/CD processes, allowing you to customize builds, deployments, and other pipeline activities.

[TOC]

## What Are YAML Variables?

YAML variables in GitLab CI/CD are key-value pairs defined in your `.gitlab-ci.yml` file or through the GitLab UI, which can be used to store and pass data throughout your pipeline stages and jobs. These variables can be used to customize the execution environment, control script behavior, manage deployments, and more.

### Types of YAML Variables

- **Predefined Variables:** GitLab provides a set of predefined variables, which are automatically available for use in your pipelines. These include information about the project, pipeline, commit, and more.
    - See https://docs.gitlab.com/ee/ci/variables/predefined_variables.html for a list of pre-defined variables.
- **Custom Variables:** You can define your own custom variables in the `.gitlab-ci.yml` file or through the GitLab project's CI/CD settings. These variables can be project-specific, protected, or masked.

### Defining YAML Variables

Variables can be defined at different levels within your `.gitlab-ci.yml` file:

- **Global Level:** Variables defined at the top level of the file apply to all jobs and stages in the pipeline.
- **Job Level:** Variables defined within a job apply only to that job.

```yaml
variables:
  GLOBAL_VAR: "global_value"

stages:
  - build
  - test

build_job:
  stage: build
  variables:
    JOB_VAR: "job_specific_value"
  script:
    - echo $GLOBAL_VAR
    - echo $JOB_VAR
```

### Using YAML Variables

Variables can be used in your `.gitlab-ci.yml` file to dynamically configure your pipeline. They are accessible within your CI/CD scripts using the standard environment variable syntax for your shell, e.g., `$VARIABLE_NAME` in Bash.

```yaml
deploy_job:
  stage: deploy
  script:
    - deploy_to_server --token $DEPLOY_TOKEN

```
### Best Practices

- **Security:** Use masked and protected variables for sensitive information, such as API keys and passwords, to prevent exposure in logs.
- **Naming:** Use clear and consistent naming conventions for your variables to make your pipeline configuration easy to understand and maintain.
- **Documentation:** Document the purpose and expected values of your custom variables, especially if they are required for pipeline execution.

## References and Anchors in YAML

A powerful feature of YAML that can be particularly useful in the context of GitLab CI/CD pipelines is the use of references and anchors. This feature allows you to define a piece of the configuration once and reuse it elsewhere in the document, which can simplify your pipeline configuration and make it easier to maintain.

### What are References and Anchors?

- **Anchors (`&`):** You can use anchors to mark a section of the configuration file that you want to reuse elsewhere.
- **Aliases (`*`):** You can use aliases to reference the anchored section elsewhere in the file.
- **Merge Key (`<<`):** The merge key is used to indicate that the elements of the anchored section should be merged into the current section.

### Using References and Anchors

References and anchors are particularly useful for reusing configurations such as environment variables, script commands, or job definitions.

#### Example: Defining Common Variables
```yaml
.common_variables: &common_variables
  VARIABLE_ONE: "value_one"
  VARIABLE_TWO: "value_two"

job1:
  variables:
    <<: *common_variables
  script:
    - echo "This is job 1."

job2:
  variables:
    <<: *common_variables
  script:
    - echo "This is job 2."

```

In this example, we define a set of common variables using an anchor named `common_variables`. These variables are then referenced in `job1` and `job2`, ensuring both jobs have access to the `VARIABLE_ONE` and `VARIABLE_TWO` without needing to define them multiple times.

#### Example: Reusing Script Commands
```yaml
.common_script: &common_script
  - echo "Preparation step."
  - echo "Another common step."

job1:
  script:
    - *common_script
    - echo "Specific step for job 1."

job2:
  script:
    - *common_script
    - echo "Specific step for job 2."

```
Here, a common set of script commands is defined with an anchor `common_script`. These commands are then included in the `script` sections of `job1` and `job2`, followed by job-specific commands.

### Best Practices

- **Clarity:** Use clear and descriptive names for your anchors to ensure that your YAML file remains readable and understandable.
- **Scope:** While references and anchors can significantly reduce duplication, use them judiciously to keep your configurations manageable and avoid confusion.
- **Documentation:** Document any non-obvious uses of anchors and references, especially when they span large sections of the file, to aid in maintainability.

## Applying Variables to our Pipeline
To use variables to store the image name in the GitLab CI/CD file, you can define a variable at the beginning of the file and then reference that variable wherever you need to specify the image. Here's how you can modify your GitLab CI/CD file to use variables:

```yaml
---
variables:
  PROJECT_IMAGE: gitlab.home.arpa:5050/developer/workshop/project-image:latest

stages:
  - lint
  - build
  - analysis
  - test

lint:
  image: $PROJECT_IMAGE
  stage: lint
  script:
    - find . -iname '*.c' -o -iname '*.h' | xargs clang-format -i --style=microsoft
    - git diff --exit-code || (echo "Code style issues found. Please run 'clang-format' locally." && exit 1)

build:
  image: $PROJECT_IMAGE
  stage: build
  script:
    - cmake -S . -B build
    - cmake --build build
  artifacts:
    paths:
      - build/
    expire_in: 1 week

analysis:
  image: $PROJECT_IMAGE
  stage: analysis
  script:
    - find . -iname '*.c' -o -iname '*.h' | xargs clang-tidy -p build --checks=*,-clang-analyzer-alpha.*

test:
  image: $PROJECT_IMAGE
  stage: test
  script:
    - ./build/main > output.txt
    - cat output.txt
    - |
      if grep -q "Hello, GitLab CI/CD!" output.txt; then
        echo "Test Passed"
      else
        echo "Test Failed"
        exit 1
      fi
```

Now, if you ever need to change the image name, you can simply modify the PROJECT_IMAGE variable at the top of the file, and it will be automatically updated wherever it's referenced throughout the file. This makes it easier to maintain and update your CI/CD configuration.

## Conclusion

By leveraging variables, references, and anchors in your `.gitlab-ci.yml` file, you can create more concise and maintainable CI/CD configurations. This feature enables you to define complex configurations once and reuse them throughout your pipeline, reducing duplication and potential errors.

For further details on advanced YAML features and their applications in GitLab CI/CD, refer to the [official YAML documentation](https://yaml.org/spec/1.2/spec.html) and the [GitLab CI/CD documentation](https://docs.gitlab.com/ee/ci/yaml/).

[Overview](../../README.md) | [Back](./03_working_with_artifacts.md) | [Next](./05_workflow_and_rules.md)
