---
variables:
  PROJECT_IMAGE: gitlab.home.arpa:5050/developer/workshop/project-image:latest
  GITLAB_CLI_IMAGE: gitlab.home.arpa:5050/developer/workshop/gitlab-release-cli:latest
  PROJECT_API: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}
  PACKAGE_ARCH: generic
  PACKAGE_NAME: ${CI_PROJECT_NAME}-${PACKAGE_VERSION}-${PACKAGE_ARCH}

stages:
  - lint
  - build
  - analysis
  - test
  - package

workflow:
  rules:
    - if: $CI_COMMIT_TAG
      variables:
        PACKAGE_VERSION: "${CI_COMMIT_TAG}"
    - if: ($CI_COMMIT_TAG == null && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH)
      variables:
        PACKAGE_VERSION: "${CI_COMMIT_BRANCH}_${CI_COMMIT_SHORT_SHA}"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        PACKAGE_VERSION: "latest"

lint:
  image: $PROJECT_IMAGE
  stage: lint
  script:
    - find . -iname '*.c' -o -iname '*.h' | xargs clang-format -i --style=microsoft
    - git diff --exit-code || (echo "Code style issues found. Please run 'clang-format' locally." && exit 1)

build:
  image: $PROJECT_IMAGE
  stage: build
  script:
    - cmake -DPACKAGE_NAME=${CI_PROJECT_NAME} -DPACKAGE_VERSION=${PACKAGE_VERSION} -DPACKAGE_ARCH=${PACKAGE_ARCH} -S . -B build --preset default
    - cmake --build build
  artifacts:
    paths:
      - build/
    expire_in: 1 week

analysis:
  image: $PROJECT_IMAGE
  stage: analysis
  script:
    - find . -iname '*.c' -o -iname '*.h' | xargs clang-tidy -p build --checks=*,-clang-analyzer-alpha.*

test:
  image: $PROJECT_IMAGE
  stage: test
  script:
    - ./build/main > output.txt
    - cat output.txt
    - |
      if grep -q "Hello, GitLab CI/CD!" output.txt; then
        echo "Test Passed" > results.txt
      else
        echo "Test Failed" > results.txt
        exit 1
      fi
  artifacts:
    paths:
      - results.txt
    expire_in: 1 week

package:
  image: $PROJECT_IMAGE
  stage: package
  script:
    - cd build
    - cpack
    - cp out/${PACKAGE_NAME}.tar.gz ..
  dependencies:
    - build
    - test
  artifacts:
    paths:
      - ${PACKAGE_NAME}.tar.gz
