# Your First GitLab CI/CD Pipeline

In this tutorial, you will learn how to set up a Continuous Integration/Continuous Deployment (CI/CD) pipeline for a C project hosted on GitLab. We will cover everything from creating a new project, writing a simple C program, setting up the CI/CD pipeline configuration, to executing the pipeline.

[TOC]

## Prerequisites
Before starting this tutorial, ensure you have the following:
- A GitLab account on the GitLab instance [gitlab.home.arpa](https://gitlab.home.arpa).
- A GitLab runner connected to the GitLab instance
- An existing project in the GitLab instance.
- A pipeline image that was built and pushed to the GitLab project container registry.
- Git installed on your local machine.
- Basic knowledge of C programming.

## Step 1: Cloning the Repository
1. Copy the repository URL provided by GitLab for the project you created.
2. Open a terminal window on your local machine.
3. Use the `git clone` command to clone the repository to your local machine:
   ```bash
   git clone <repository-url>
   ```
   Replace `<repository-url>` with the URL you copied from GitLab. It should look like `https://gitlab.home.arpa/developer/workshop.git`.

## Step 2: Creating a Simple C Program
1. Navigate to the directory where you cloned the repository.
2. Create a new file named `main.c`.
3. Open `main.c` in a text editor and add the following simple C program:
```C
#include <stdio.h>

int main(void) {
      printf("Hello, GitLab CI/CD!\n");
      return 0;
}
```

## Step 3: Writing the GitLab CI/CD Configuration
1. In the root directory of your project, create a new file named `.gitlab-ci.yml`.
2. Open `.gitlab-ci.yml` in a text editor and add the following configuration:
```yaml
---
stages:
- build
- test

build:
   image: gitlab.home.arpa:5050/developer/workshop/project-image:latest
   stage: build
   script:
      - cmake -S . -B build
      - cmake --build build

test:
   image: gitlab.home.arpa:5050/developer/workshop/project-image:latest
   stage: test
   script:
      - ./build/main > output.txt
      - cat output.txt
      - |
      if grep -q "Hello, GitLab CI/CD!" output.txt; then
      echo "Test Passed"
      else
      echo "Test Failed"
      exit 1
      fi
```

## Step 4: Pushing Changes to GitLab
1. Add the `main.c` and `.gitlab-ci.yml` files to the staging area:
 ```bash
 git add main.c .gitlab-ci.yml
 ```

1. Commit the changes with a descriptive message:
 ```bash
 git commit -m "Add main.c and .gitlab-ci.yml"
 ```

1. Push the changes to GitLab:
 ```bash
 git push origin main
 ```

## Step 5: Executing the Pipeline
1. Once you've pushed the changes, GitLab CI/CD will automatically detect the `.gitlab-ci.yml` file and start executing the pipeline.
2. The pipeline will go through the defined stages (`build` and `test`) sequentially.
3. You can monitor the progress of the pipeline in the GitLab UI under your project's CI/CD section.
4. Upon successful execution, you should see the output of the `main.c` program in the pipeline logs.

## Conclusion
Congratulations! You've successfully set up your first GitLab CI/CD pipeline for a C project. You've learned how to create a simple C program, write a GitLab CI/CD configuration, and execute the pipeline on GitLab. This pipeline will automatically build, test, and verify your C project whenever changes are pushed to the repository.


## Resources

For further reading and more advanced topics, check out the following resources:

- [GitLab CI/CD Documentation](https://docs.gitlab.com/ee/ci/)
- [YAML Syntax](https://yaml.org/spec/1.2/spec.html)
- [GitLab CI/CD Examples](https://docs.gitlab.com/ee/ci/examples/)

[Overview](../../README.md) | [Back](../01_getting_started/03_introduction_to_gitlab.md) | [Next](./02_adding_additional_stages.md)
