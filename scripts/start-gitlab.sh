#!/usr/bin/env bash

# exit on any errors
set -e

# start all gitlab services
sudo docker compose up -d
