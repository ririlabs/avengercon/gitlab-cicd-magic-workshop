#!/usr/bin/env bash

# exit on any errors
set -e

gitlab_runner_cmd="gitlab-runner register \
	--non-interactive \
	--executor docker \
	--docker-image ubuntu:latest \
	--name $1 \
	--tls-ca-file=/etc/gitlab-runner/certs/gitlab.home.arpa.CA.crt \
	--url https://gitlab.home.arpa \
	--docker-extra-hosts "gitlab.home.arpa:172.17.0.1" \
	--token $2"

sudo docker exec -it $1 bash -c "$gitlab_runner_cmd"
