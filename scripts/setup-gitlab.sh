#!/usr/bin/env bash

# exit on any errors
set -e

echo "Setting up GitLab EE..."

# generate self-signed cert
echo "Generating self-signed certificate..."
mkdir -p ssl
openssl genrsa -out ./ssl/gitlab.home.arpa.CA.key 4096
openssl req -x509 -new -nodes \
	-key ./ssl/gitlab.home.arpa.CA.key -sha256 \
	-days 3650 -out ./ssl/gitlab.home.arpa.CA.crt \
	-subj '/CN=gitlab.home.arpa/C=US/ST=GA/L=Augusta/O=AvengerCon'
openssl req -new -nodes -out ./ssl/gitlab.home.arpa.csr \
	-newkey rsa:4096 \
	-keyout ./ssl/gitlab.home.arpa.key \
	-subj '/CN=gitlab.home.arpa/C=US/ST=GA/L=Augusta/O=AvengerCon'

cat > ./ssl/gitlab.v3.ext << EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = gitlab.home.arpa
IP.1 = 127.0.0.1
EOF

openssl x509 -req -in ./ssl/gitlab.home.arpa.csr \
	-CA ./ssl/gitlab.home.arpa.CA.crt -CAkey ./ssl/gitlab.home.arpa.CA.key -CAcreateserial \
	-out ./ssl/gitlab.home.arpa.crt -days 825 -sha256 -extfile ./ssl/gitlab.v3.ext

sudo cp ./ssl/gitlab.home.arpa.CA.crt /usr/local/share/ca-certificates/
sudo update-ca-certificates
cp ./ssl/gitlab.home.arpa.CA.crt ../docker/

# setup gitlab config
gitlab_home=/srv/gitlab

sudo echo -e "Host gitlab.home.arpa\nPort 2222" | sudo tee -a /etc/ssh/ssh_config

echo "Creating gitlab config folders..."
sudo mkdir -p $gitlab_home/runner-one/config/certs
sudo mkdir -p $gitlab_home/runner-two/config/certs
sudo mkdir -p $gitlab_home/runner-priv/config/certs
sudo mkdir -p $gitlab_home/config/ssl

echo "Creating docker compose .env file..."
echo "GITLAB_HOME=$gitlab_home" > .env

echo "Pulling GitLab docker images..."
sudo sed -i "/127.0.0.1\tlocalhost/c\127.0.0.1\tlocalhost gitlab.home.arpa" /etc/hosts

# start gitlab and gitlab-runner to copy certs and do initial setup
sudo docker compose pull
sudo docker compose up -d

sudo cp ./ssl/gitlab.home.arpa.key ./ssl/gitlab.home.arpa.crt $gitlab_home/config/ssl/
sudo cp ./ssl/gitlab.home.arpa.CA.crt $gitlab_home/runner-one/config/certs
sudo cp ./ssl/gitlab.home.arpa.CA.crt $gitlab_home/runner-two/config/certs
sudo cp ./ssl/gitlab.home.arpa.CA.crt $gitlab_home/runner-priv/config/certs

# restart gitlab and gitlab-runner and finalize setup
sudo docker compose restart

echo "Getting initial GitLab root password..."
until sudo docker exec gitlab grep 'Password:' /etc/gitlab/initial_root_password
do
  echo "waiting for gitlab initial root password to be generated..."
  sleep 1
done

echo "Setup complete! You can now access GitLab at https://gitlab.home.arpa."
