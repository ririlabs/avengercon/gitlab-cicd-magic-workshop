FROM ubuntu:22.04

RUN apt-get update && apt-get install -y --no-install-recommends git cmake build-essential jq python3-dev jsonnet cppcheck clang clang-tidy clang-format python3-pip ninja-build curl && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN pip3 install --upgrade pip && pip3 install pytest pytest-html black pylint build virtualenv twine pyinstaller
COPY gitlab.home.arpa.CA.crt /usr/local/share/ca-certificates/
RUN update-ca-certificates
