FROM auchida/pandoc:latest

COPY gitlab.home.arpa.CA.crt /usr/local/share/ca-certificates/
RUN update-ca-certificates
