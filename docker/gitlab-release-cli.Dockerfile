FROM golang:latest as build-release-cli

RUN git clone https://gitlab.com/gitlab-org/release-cli.git
RUN make -C release-cli
RUN cp release-cli/bin/release-cli /usr/local/bin

FROM alpine:latest

RUN apk --no-cache add ca-certificates curl jq twine
COPY gitlab.home.arpa.CA.crt /usr/local/share/ca-certificates/
COPY --from=build-release-cli /usr/local/bin/release-cli /usr/local/bin
RUN update-ca-certificates
