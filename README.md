# GitLab CICD Magic Workshop

This workshop will cover a wide range of topics about GitLab CI/CD and how it applies to developing, testing, and releasing products. The workshop will take you from zero to hero on pipeline development and proper use of GitLab features and resources.

[TOC]

## Lesson 01 - Getting Started
- [Environment Setup](./lessons/01_getting_started/01_environment_setup.md)
- [Development Philosophy](./lessons/01_getting_started/02_development_philosophy.md)
- [Introduction to GitLab](./lessons/01_getting_started/03_introduction_to_gitlab.md)
- [How GitLab Tokens Work](./lessons/01_getting_started/04_gitlab_tokens.md)

## Lesson 02 - Basic Pipeline Concepts
- [Your First GitLab CI/CD Pipeline](./lessons/02_basic_pipelines/01_your_first_pipeline.md)
- [Adding Additional Stages](./lessons/02_basic_pipelines/02_adding_additional_stages.md)
- [Working With GitLab CI/CD Artifacts](./lessons/02_basic_pipelines/03_working_with_artifacts.md)
- [Using YAML Variables, Anchors, and References](./lessons/02_basic_pipelines/04_yaml_variables_anchors_references.md.md)
- [Using the workflow and rules Keywords](./lessons/02_basic_pipelines/05_workflow_and_rules.md)
- [Adding a Packaging Stage](./lessons/02_basic_pipelines/06_adding_packaging_stage.md)
- [Adding a Release Stage](./lessons/02_basic_pipelines/07_adding_release_stage.md)

## Lesson 03 - Optimizing and Improving Maintainability
- [Using `needs` vs `dependencies` Keywords](./lessons/03_optimizing_and_maintanability/01_using_needs_keyword.md)
- [Using the `extends` Keyword and `!reference` Tags](./lessons/03_optimizing_and_maintanability/02_using_extends_and_reference.md)
- [Using the `include` Keyword](./lessons/03_optimizing_and_maintanability/03_using_include.md)
- [Using the `parallel` Keyword](./lessons/03_optimizing_and_maintanability/04_using_parallel.md)
- [Downstream and Multi-Project Pipelines](./lessons/03_optimizing_and_maintanability/05_downstream_and_mp_pipelines.md)
- [Creating Dynamic Pipelines](./lessons/03_optimizing_and_maintanability/06_dynamic_pipelines.md)

## Lesson 04 - Advanced Pipeline Concepts
- [Using Scheduled Pipelines](./lessons/04_advanced_pipelines/01_scheduled_pipelines.md)
- [Building and Publishing Docker Images](./lessons/04_advanced_pipelines/02_build_and_publish_docker.md)
- [Building and Publishing Python packages](./lessons/04_advanced_pipelines/03_build_and_publish_python.md)
- [GitLab CI/CD Components](./lessons/04_advanced_pipelines/04_gitlab_cicd_components.md)
- [Deploy Environments and Resource Groups](./lessons/04_advanced_pipelines/05_deploy_environments.md)

## Lesson 05 - Applying CI/CD Automation to Real Products
- [Creating a User Guide from README.md](./lessons/05_applying_to_products/01_user_guide_creation.md)
- [Requirements Testing and Creating a Test Report](./lessons/05_applying_to_products/02_requirements_testing.md)

The workshop will conclude with live discussions, a Q & A session, and any other topics the participants want to cover (time permitting).
